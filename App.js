import { AppLoading } from 'expo';
import { Asset } from 'expo-asset';
import * as Font from 'expo-font';
import React, { useState } from 'react';
import { Platform, StatusBar, StyleSheet, View, SafeAreaView,Text  } from 'react-native';
import FlashMessage from "react-native-flash-message";
// import { Ionicons } from '@expo/vector-icons';
import { Provider } from 'react-redux';



//Components - Controllers
import ViewController from './controllers/ViewController';
import GeneralStatusBarColor from './components/GeneralStatusBarColor';
import {store} from "./controllers/ShareDataController";


export default function App(props) {
  const [isLoadingComplete, setLoadingComplete, statusNew] = useState(false);

  if (!isLoadingComplete && !props.skipLoadingScreen && !statusNew) {
    console.disableYellowBox = true;
    return (
      <AppLoading
        startAsync={loadResourcesAsync}
        onError={handleLoadingError}
        onFinish={() => handleFinishLoading(setLoadingComplete)}
      />
    );
  } else {
    return (
      <Provider store={store}>
        <View style={{flex:1}}> 
        <GeneralStatusBarColor backgroundColor={"#C5E0B4"} animated={true} barStyle="light-content" />
        <SafeAreaView style={styles.container}>
          <ViewController/>
          
        </SafeAreaView>
        <FlashMessage position="top" animated={true} />
        </View>
      </Provider>
    );
  }
}


async function loadResourcesAsync() {
  await Promise.all([
    Asset.loadAsync([
      require('./assets/images/robot-dev.png'),
      require('./assets/images/robot-prod.png'),
    ]),
    Font.loadAsync({
      // This is the font that we are using for our tab bar
      // ...Ionicons.font,
      // We include SpaceMono because we use it in HomeScreen.js. Feel free to
      // remove this if you are not using it in your app
      'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
    }),
  ]);
}

function handleLoadingError(error) {
  // In this case, you might want to report the error to your error reporting
  // service, for example Sentry
  console.warn(error);
}

function handleFinishLoading(setLoadingComplete) {
  setLoadingComplete(true);
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
