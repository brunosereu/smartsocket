import { AsyncStorage, } from "react-native";

function UpdateValues(regua,dadosAtualizados){
    // console.log('oi');

    regua.consumoEsteMes = dadosAtualizados.json.WattsTotal;
    regua.tomadas[0].consumo = dadosAtualizados.json.WattsT1;
    regua.tomadas[1].consumo = dadosAtualizados.json.WattsT2;
    regua.custoPrevisto = dadosAtualizados.json.RSpKhwTotal;
    regua.lastUpdate = (new Date().getDate()).toString().padStart(2, "0") + '/' + (new Date().getMonth() + 1).toString().padStart(2, "0") + '/' + new Date().getFullYear() + ' ' + new Date().getHours().toString().padStart(2, "0") + ':' + new Date().getMinutes().toString().padStart(2, "0")
    // console.log(regua);
    AsyncStorage.setItem('@Regua',JSON.stringify(regua));
    return {
        type:'UPDATE_VALUES',
        regua:regua
    };
};

function UpdateValuesGraficoSemanal(regua,dadosGraficoSemanal){
    // console.log(dadosGraficoSemanal.json);
    regua.dadosGraficoSemanal = dadosGraficoSemanal.json;
    // regua.dadosGraficoSemanal = [];
    regua.lastUpdate = (new Date().getDate()).toString().padStart(2, "0") + '/' + (new Date().getMonth() + 1).toString().padStart(2, "0") + '/' + new Date().getFullYear() + ' ' + new Date().getHours().toString().padStart(2, "0") + ':' + new Date().getMinutes().toString().padStart(2, "0")
    // console.log(regua);
    AsyncStorage.setItem('@Regua',JSON.stringify(regua));
    return {
        type:'UPDATE_VALUES_GRAFICO_SEMANAL',
        regua:regua
    };
};


function UpdateNomeRegua(regua,novoNome){
    regua.nome = novoNome.length < 5 ? "Regua" : novoNome; 
    AsyncStorage.setItem('@Regua',JSON.stringify(regua));
    return {
        type:'UPDATE_REGUA_NOME',
        regua:regua
    };
}


function AddTimer(regua,dadosTimer){
    // regua.timers = []
    regua.timers.push(dadosTimer)
    AsyncStorage.setItem('@Regua',JSON.stringify(regua));
    return {
        type:'ADD_TIMER',
        regua:regua
    };
}

function UpdateTimer(regua,dadosTimer){
    id = regua.timers.findIndex((e)=>{ return e.idTimer == dadosTimer.idTimer });
    regua.timers[id] = dadosTimer
    AsyncStorage.setItem('@Regua',JSON.stringify(regua));
    return {
        type:'UPDATE_TIMER',
        regua:regua
    };
}

function UpdateStatusTimer(regua,dadosTimer){
    // console.log(regua.timers)
    id = regua.timers.findIndex((e)=>{ return e.idTimer == dadosTimer.idTimer });
    regua.timers[id].status = dadosTimer.status == 0 ? 1 : 0
    AsyncStorage.setItem('@Regua',JSON.stringify(regua));
    return {
        type:'UPDATE_STATUS_TIMER',
        regua:regua
    };
}

function DeleteTimer(regua,dadosTimer){
    id = regua.timers.findIndex((e)=>{ return e.idTimer == dadosTimer.idTimer });
    regua.timers.splice(id,1);
    AsyncStorage.setItem('@Regua',JSON.stringify(regua));
    return {
        type:'DELETE_TIMER',
        regua:regua
    };
}

function UpdateStatusTomada(regua,tomada){
    regua.tomadas[tomada.key-1].status = !tomada.status; 
    regua.tomadas[tomada.key-1].nome = tomada.nome; 
    AsyncStorage.setItem('@Regua',JSON.stringify(regua));
    return {
        type:'UPDATE_STATUS_TOMADA',
        regua:regua
    };
}

function UpdateNomeTomada(regua,tomada){
    regua.tomadas[tomada.key-1].nome = tomada.nome; 
    AsyncStorage.setItem('@Regua',JSON.stringify(regua));
    return {
        type:'UPDATE_NOME_TOMADA',
        regua:regua
    };
}

function UpdateStatusOnlineTomada(regua,updateStatus){
    // console.log(updateStatus)
    regua.tomadas[0].status = !(!!Number(updateStatus.statusTomada1)); 
    regua.tomadas[1].status = !(!!Number(updateStatus.statusTomada2)); 
    // console.log(regua.tomadas);
    AsyncStorage.setItem('@Regua',JSON.stringify(regua));
    return {
        type:'UPDATE_STATUS_TOMADA_ONLINE',
        regua:regua
    };
}

//Usuario
function Login(usuario,DataForUser){
    // console.log(updateStatus)
    // regua.tomadas[0].status = !(!!Number(updateStatus.statusTomada1)); 
    // regua.tomadas[1].status = !(!!Number(updateStatus.statusTomada2)); 
    usuario.logado = true

    usuario.id = DataForUser.idUsuario
    usuario.email = DataForUser.email
    usuario.nome = DataForUser.username
    usuario.comRegua = DataForUser.temRegua == 0 ? false : true

    // console.log(regua.tomadas);
    AsyncStorage.setItem('@User',JSON.stringify(usuario));
    return {
        type:'Login',
        usuario:usuario
    };
}
function Logout(usuario){
    // console.log(updateStatus)
    // regua.tomadas[0].status = !(!!Number(updateStatus.statusTomada1)); 
    // regua.tomadas[1].status = !(!!Number(updateStatus.statusTomada2)); 
    usuario.logado = false

    // console.log(regua.tomadas);
    AsyncStorage.setItem('@User',JSON.stringify(usuario));
    return {
        type:'Logout',
        usuario:usuario
    };
}

function EditUser(usuario,DataForUser){
    // console.log(updateStatus)
    // regua.tomadas[0].status = !(!!Number(updateStatus.statusTomada1)); 
    // regua.tomadas[1].status = !(!!Number(updateStatus.statusTomada2)); 
    usuario.email = DataForUser.email
    usuario.nome = DataForUser.username

    // console.log(regua.tomadas);
    AsyncStorage.setItem('@User',JSON.stringify(usuario));
    return {
        type:'Login',
        usuario:usuario
    };
}

function ConfigurarRegua(usuario){
    usuario.comRegua = true


    AsyncStorage.setItem('@User',JSON.stringify(usuario));
    return {
        type:'Login',
        usuario:usuario
    };
}

function DesconfigurarRegua(usuario){
    usuario.comRegua = false


    AsyncStorage.setItem('@User',JSON.stringify(usuario));
    return {
        type:'Login',
        usuario:usuario
    };
}


export {UpdateValues,UpdateValuesGraficoSemanal,UpdateStatusOnlineTomada,UpdateNomeRegua,UpdateStatusTomada,UpdateNomeTomada,
    AddTimer,UpdateTimer,DeleteTimer,UpdateStatusTimer,Login,Logout,EditUser,ConfigurarRegua,DesconfigurarRegua}