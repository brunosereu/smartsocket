import { combineReducers } from 'redux';

const initialState = { 
    regua :{
        nome:'',
        custoPrevisto:'',
        consumoEsteMes:0,
        tomadas:[ ],
        timers:[  ],
        lastUpdate: (new Date().getDate()).toString().padStart(2, "0") + '/' + (new Date().getMonth() + 1).toString().padStart(2, "0") + '/' + new Date().getFullYear() + ' ' + new Date().getHours().toString().padStart(2, "0") + ':' + new Date().getMinutes().toString().padStart(2, "0"),
        dadosGraficoSemanal: [ ]
    },
    usuario:{
        nome:'',
        email:'',
        logado:false,
        comRegua:false
    }
};


function regua(state=initialState.regua,action){
    // console.log(state);
    switch (action.type) {
        case 'UPDATE_VALUES':
            return action.regua
        case 'UPDATE_REGUA_NOME':
            return action.regua
        case 'UPDATE_STATUS_TOMADA':
            return action.regua
        case 'UPDATE_NOME_TOMADA':
            return action.regua
        case 'UPDATE_VALUES_GRAFICO_SEMANAL':
            return action.regua
        case 'UPDATE_STATUS_TOMADA_ONLINE':
            return action.regua
        case 'UPDATE_STATUS_TIMER':
            return action.regua
        case 'UPDATE_TIMER':
            return action.regua
        case 'DELETE_TIMER':
            return action.regua
        case 'ADD_TIMER':
            return action.regua
        case 'setInit':
            return action.regua
        default:
            return state;
    }
}
function usuario(state=initialState.usuario,action){
    // console.log(state);
    switch (action.type) {
        case 'setInitUser':
            return action.usuario
        case 'Login':
            return action.usuario
        case 'ReguaConfigurada':
            return action.usuario
        case 'Logout':
            return action.usuario
        case 'ReguaDesconfigurada':
            return action.usuario
        default:
            return state;
    }
}

export default combineReducers({
    regua,
    usuario
})