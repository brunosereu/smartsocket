import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
  } from 'react-native';
import HeaderComponnent from '../components/HeaderComponnent';
import AppNavigator from '../navigation/AppNavigator';
import LoginNavigator from '../navigation/Login/LoginNavigator';
import WithouReguaNavigator from '../navigation/WithouRegua/WithouReguaNavigator';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import GeneralStatusBarColor from '../components/GeneralStatusBarColor';

import { showMessage, hideMessage } from "react-native-flash-message";

import * as Actions from '../redux/actions/ReduxActionController';

class ViewController extends Component {
    
    constructor(props) {
        super(props);
        // this.regua 
        // this.state = {
        //     regua:this.regua
        // };
    }
    


    render() {
        return (
            <View style={{flex:1}}>
               { this.props.usuario.logado == true?

                    this.props.usuario.comRegua == true?
                        <View style={{flex:1}}>
                            <AppNavigator /> 
                        </View>
                        :
                        <View style={{flex:1}}>
                            <WithouReguaNavigator /> 
                            <GeneralStatusBarColor backgroundColor={"#FFFFFF"} animated={true} barStyle="dark-content" />
                        </View>
                :
                <View style={{flex:1,backgroundColor:'red'}}>
                    
                    <LoginNavigator /> 
                    <GeneralStatusBarColor backgroundColor={"#FFFFFF"} animated={true} barStyle="dark-content" />
                </View>
              
              }
                
            </View>
        );
    }
}

const mapStateToProps = state => ({ regua:state.regua,usuario:state.usuario });

const mapDispatchToProps = dispatch =>  bindActionCreators(Actions, dispatch);


export default connect(mapStateToProps,mapDispatchToProps)(ViewController);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
      },
});
  