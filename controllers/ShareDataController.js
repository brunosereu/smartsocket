import {createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { AsyncStorage, } from "react-native";

import ReduxReducerController from '../redux/reducers/ReduxReducerController';

const initialState = { 
    regua :{
        nome:'Regua',
        custoPrevisto:'0,00',
        consumoEsteMes:0,
        tomadas:[
            {
                key:1,
                nome:'Tomada 1',
                consumo:0,
                status:true
            },
            {
                key:2,
                nome:'Tomada 2',
                consumo:0,
                status:true
            },
        ],
        timers:[ ],
        lastUpdate: (new Date().getDate()).toString().padStart(2, "0") + '/' + (new Date().getMonth() + 1).toString().padStart(2, "0") + '/' + new Date().getFullYear() + ' ' + new Date().getHours().toString().padStart(2, "0") + ':' + new Date().getMinutes().toString().padStart(2, "0"),
        dadosGraficoSemanal: []
    },
    usuario:{
        id:'',
        nome:'',
        email:'',
        logado:false,
        comRegua:false
    }
}; 

const store = createStore(ReduxReducerController,applyMiddleware(thunk));

export  { store };

const setInit = (regua) => {
    return {
      type: "setInit",
      regua: regua,
    };
} 

const setInitUser = (usuario) => {
    return {
      type: "setInitUser",
      usuario: usuario,
    };
} 
  
const getAsyncStorage = () => {
    return (dispatch) => {
        AsyncStorage.getItem('@Regua')
        .then((result) => {
            if(!result){
                AsyncStorage.setItem('@Regua',JSON.stringify(initialState.regua))
                valReturned = initialState.regua;
            }else{
                valReturned = JSON.parse(result)
            }
            // console.log(valReturned)
            dispatch(setInit(valReturned))
        });
    };
};
  
const getAsyncStorageUser = () => {
    return (dispatch) => {
        AsyncStorage.getItem('@User')
        .then((result) => {
            if(!result){
                AsyncStorage.setItem('@User',JSON.stringify(initialState.usuario))
                valReturnedUser = initialState.usuario;
            }else{
                valReturnedUser = JSON.parse(result)
            }
            dispatch(setInitUser(valReturnedUser))
        });
    };
};
// Dispatch the getAsyncStorage() action directly on the store.
store.dispatch(getAsyncStorageUser());
store.dispatch(getAsyncStorage());
