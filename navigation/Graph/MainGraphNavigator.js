import React, { Component } from 'react';
import { Platform } from 'react-native';
import { View, Text, StyleSheet, Button } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import FontAWIcon from '../../components/FontAWIcon';
import GraphScreen from '../../screens/GraphScreen';
import CustomGraph from '../../screens/CustomGraphScreen';

const config = {
  navigationOptions:{
    headerMode: 'none'
  }
};


const GraphStack = createStackNavigator(
  {
    Graph: GraphScreen,
    CustomGraph: CustomGraph
  },
  {
    defaultNavigationOptions: {
      headerMode: 'none',
      headerVisible: false,
      header: null,
    },
    navigationOptions: {
      headerMode: 'none',
      headerVisible: false,
      header: null,
    },
  }
);

GraphStack.navigationOptions = {
  tabBarLabel: 'Graph',
  tabBarIcon: ({ focused }) => (
    <FontAWIcon focused={focused} bottomBar={true} name={'chart-bar'} />
  ),
};

GraphStack.path = '/Graph';

const tabNavigator = createBottomTabNavigator({
  GraphStack,
},{
  tabBarOptions: {
    showLabel: false,
    style: {
      padding: 10,
      height: 80,
    },
  },
});

tabNavigator.path = '';

export default GraphStack;
