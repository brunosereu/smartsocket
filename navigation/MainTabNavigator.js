import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import TabBarIcon from '../components/TabBarIcon';
import FontAWIcon from '../components/FontAWIcon';
import HomeScreen from '../screens/HomeScreen';
import TimerScreen from '../screens/TimerScreen';
import MainGraphNavigator from './Graph/MainGraphNavigator';
import Graph from '../screens/GraphScreen';

const config = {
  navigationOptions:{
    headerMode: 'none'
  }
};

const HomeStack = createStackNavigator(
  {
    Home: HomeScreen,
  },
  {
    defaultNavigationOptions: {
      headerMode: 'none',
      headerVisible: false,
      header: null,
    },
    navigationOptions: {
      headerMode: 'none',
      headerVisible: false,
      header: null,
    },
  }
);

HomeStack.navigationOptions = {
  tabBarLabel: 'Home',
  tabBarIcon: ({ focused }) => (
    <FontAWIcon focused={focused} bottomBar={true} name={'home'}
    />
  ),
};

HomeStack.path = '/home';

const TimerStack = createStackNavigator(
  {
    Timer: TimerScreen,
  },
  {
    defaultNavigationOptions: {
      headerMode: 'none',
      headerVisible: false,
      header: null,
    },
    navigationOptions: {
      headerMode: 'none',
      headerVisible: false,
      header: null,
    },
  }
);

TimerStack.navigationOptions = {
  tabBarLabel: 'Timer',
  tabBarIcon: ({ focused }) => (
    <FontAWIcon focused={focused} bottomBar={true} name={'stopwatch'} />
  ),
};

TimerStack.path = '/timer';


const GraphStack = createStackNavigator(
  {
    Graph: MainGraphNavigator,
  },
  {
    defaultNavigationOptions: {
      headerMode: 'none',
      headerVisible: false,
      header: null,
    },
    navigationOptions: {
      headerMode: 'none',
      headerVisible: false,
      header: null,
    },
  }
);

GraphStack.navigationOptions = {
  tabBarLabel: 'Graph',
  tabBarIcon: ({ focused }) => (
    <FontAWIcon focused={focused} bottomBar={true} name={'chart-bar'} />
  ),
};

GraphStack.path = '/Graph';

const tabNavigator = createBottomTabNavigator({
  HomeStack,
  TimerStack,
  GraphStack,
},{
  tabBarOptions: {
    showLabel: false,
    style: {
      padding: 10,
      height: 80,
    },
  },
});

tabNavigator.path = '';

export default tabNavigator;
