import React, { Component } from 'react';
import { Platform } from 'react-native';
import { View, Text, StyleSheet, Button } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import FontAWIcon from '../../components/FontAWIcon';
import LoginPage from '../../screens/Login/LoginPage';
import ForgotPasswordPage from '../../screens/Login/ForgotPasswordPage';
import CadastroFirstPage from '../../screens/Login/CadastroFirstPage';
import CadastroSecondPage from '../../screens/Login/CadastroSecondPage';
import CadastroThirdPage from '../../screens/Login/CadastroThirdPage';

const config = {
  navigationOptions:{
    headerMode: 'none'
  }
};


const LoginStack = createStackNavigator(
  {
    Login: LoginPage,
    ForgotPasswordPage: ForgotPasswordPage,
    CadastroFirstPage:CadastroFirstPage,
    CadastroSecondPage:CadastroSecondPage,
    CadastroThirdPage:CadastroThirdPage
  },
  {
    defaultNavigationOptions: {
      headerMode: 'none',
      headerVisible: false,
      header: null,
    },
    navigationOptions: {
      headerMode: 'none',
      headerVisible: false,
      header: null,
    },
  }
);

LoginStack.navigationOptions = {
  tabBarLabel: 'Graph',
  tabBarIcon: ({ focused }) => (
    <FontAWIcon focused={focused} bottomBar={true} name={'chart-bar'} />
  ),
};

LoginStack.path = '/Login';


export default createAppContainer(
  createSwitchNavigator({
    // You could add another route here for authentication.
    // Read more at https://reactnavigation.org/docs/en/auth-flow.html
    Login: LoginStack,
  })
);
