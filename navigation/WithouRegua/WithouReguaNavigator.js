import React, { Component } from 'react';
import { Platform } from 'react-native';
import { View, Text, StyleSheet, Button } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import FontAWIcon from '../../components/FontAWIcon';
import IndexPage from '../../screens/WithouRegua/IndexPage';
import AdicionarReguaPage from '../../screens/WithouRegua/AdicionarReguaPage';
import EditAccountPage from '../../screens/WithouRegua/EditAccountPage';

const config = {
  navigationOptions:{
    headerMode: 'none'
  }
};


const WithoutReguaStack = createStackNavigator(
  {
    IndexPage: IndexPage,
    AdicionarReguaPage: AdicionarReguaPage,
    EditAccountPage:EditAccountPage,
  },
  {
    defaultNavigationOptions: {
      headerMode: 'none',
      headerVisible: false,
      header: null,
    },
    navigationOptions: {
      headerMode: 'none',
      headerVisible: false,
      header: null,
    },
  }
);

WithoutReguaStack.navigationOptions = {
  tabBarLabel: 'Graph',
  tabBarIcon: ({ focused }) => (
    <FontAWIcon focused={focused} bottomBar={true} name={'chart-bar'} />
  ),
};

WithoutReguaStack.path = '/SemRegua';


export default createAppContainer(
  createSwitchNavigator({
    // You could add another route here for authentication.
    // Read more at https://reactnavigation.org/docs/en/auth-flow.html
    WithoutRegua: WithoutReguaStack,
  })
);
