import * as WebBrowser from 'expo-web-browser';
import React, {Component,useState } from 'react';
import {
  Image,
  TextInput,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions
} from 'react-native';

import {
  LineChart,
} from "react-native-chart-kit";
import { Rect, Text as TextSVG, Svg } from "react-native-svg";

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Moment from 'moment';



import FontAWIcon from '../../components/FontAWIcon';
import HeaderComponnent from '../../components/HeaderComponnent';
import ExtratoBlock from '../../components/ExtratoBlock';
import { showMessage, hideMessage } from "react-native-flash-message";

class CadastroFirstPage extends Component {

  constructor(props){
    super(props);
    this.scrollRef =  React.createRef()
    this.pinInterval = null
    this.props.navigation.getParam()
    // console.log(this.props.navigation)
    // console.log(this.CustomGraph)
    // this.scrollListReftop.scrollTo({x: 0, y: 0, animated: true})
  }
  state = {
  
  };


  customround (number, precision) {
      var factor = Math.pow(10, precision);
      var tempNumber = number * factor;
      var roundedTempNumber = Math.round(tempNumber);
      return roundedTempNumber / factor;
  };

  onChangeText(value){
    this.setState(value)
  }

  validateFirstPage(){
    // console.log(this.state)
    if(this.state.email == null){
      showMessage({
        message: "Verifique",
        description: "Preencha o campo de email",
        duration:2500,
        type: "default",
        backgroundColor: "#E77010", // background color
        color: "#ffff", // text color
      });
      return false
    }
    let email = this.state.email
    var ee = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);
    if(!ee.test(email) ) { 
      // alert('Preencha o campo email com um e-mail valido'); 
      showMessage({
        message: "Verifique",
        description: "Preencha o campo email com um e-mail valido",
        duration:2500,
        type: "default",
        backgroundColor: "#E77010", // background color
        color: "#ffff", // text color
      });
      return false; 
    }
    if(this.state.username == null){
      showMessage({
        message: "Verifique",
        description: "Preencha o campo de nome de usuário",
        duration:2500,
        type: "default",
        backgroundColor: "#E77010", // background color
        color: "#ffff", // text color
      });
      return false
    }
    var en = new RegExp(/^[A-Za-z0-9]*$/);
    let username = this.state.username
    if(!en.test(username) ) { 
      // alert('Preencha o campo email com um e-mail valido'); 
      showMessage({
        message: "Verifique",
        description: "Preencha o campo de nome de usuário com apenas números ou letras",
        duration:2500,
        type: "default",
        backgroundColor: "#E77010", // background color
        color: "#ffff", // text color
      });
      return false; 
    }
    this.props.navigation.navigate('CadastroSecondPage',{
      email,
      username
    })
    return false
  }

  
  render(){

      this.regua = this.props.regua;
      return (
        <View style={styles.container}>
          <View style={styles.headerContainer}>
                <View style={{  flex:1,flexDirection: 'row' }}>
                      <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop:20}}>
                        <TouchableOpacity   onPress={() => {
                            this.props.navigation.goBack()
                        }}> 
                          <FontAWIcon color={'#000000'} name={'chevron-left'} size={20} style={{marginTop:0}} />
                        </TouchableOpacity>
                      </View>
                      <View style={{flexDirection: 'row', justifyContent:'center',marginTop:20,marginLeft:110}}>
                        <Text style={styles.HEtextValorConsumo}>Cadastro</Text>
                      </View>
                </View>
          </View>
          <View style={styles.bodyContainer}>
                <View style={{  flex:1, }}>
                  <View style={{flexDirection: 'column', justifyContent: 'center'}}>
                    <View style={{flexDirection: 'row', justifyContent:'center',marginBottom:30,marginLeft:0}}>
                        <Text style={{fontSize:20, fontWeight:'bold',}}>Crie sua conta</Text>
                      </View>
                    <View style={{flexDirection: 'row',justifyContent: 'center',alignItems: 'center',borderWidth: 2,borderColor: '#000',borderRadius: 15, margin: 10,
    height: 40, borderColor: '#707070'}}>
                        <FontAWIcon color={'gray'} name={'envelope'} size={20}  style={{marginLeft: 20}}/>
                        <TextInput style={{ height: 40,padding:10,width:285,fontSize:12 }}
                            onChangeText={(valueText) =>{
                                this.onChangeText({email:valueText})
                            }}
                            value={this.state.text}
                            maxLength={256}
                            placeholder='Email'
                        />
                    </View>
                    <View style={{flexDirection: 'row',justifyContent: 'center',alignItems: 'center',borderWidth: 2,borderColor: '#000',borderRadius: 15, margin: 10,
    height: 40, borderColor: '#707070'}}>
                        <FontAWIcon color={'gray'} name={'user'} size={20}  style={{marginLeft: 20}}/>
                        <TextInput style={{ height: 40,padding:10,width:285,fontSize:12 }}
                            onChangeText={(valueText) =>{
                                this.onChangeText({username:valueText})
                            }}
                            value={this.state.text}
                            maxLength={10}
                            placeholder='Nome de Usuário (Apenas números ou letras)'
                        />
                    </View>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between',padding:20,paddingTop:40,paddingLeft:10}}>
                        <TouchableOpacity onPressIn={()=>{
                            this.validateFirstPage()
                        }}> 
                            <View style={{
                                    height:45,
                                    width:310,
                                    backgroundColor:'#E77010',
                                    borderRadius:10,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}>
                            
                                    <Text style={{ fontSize:16,color:'#ffffff',fontWeight:'bold' }}>Próximo</Text>
                            
                            </View> 
                        </TouchableOpacity>
                    </View>
                  </View>
                </View>
          </View>
    </View>
      );
    }
}

const mapStateToProps = state => ({ regua:state.regua,usuario:state.usuario });

export default connect(mapStateToProps,null)(CadastroFirstPage);

CadastroFirstPage.navigationOptions = {
  header: null,
};

const styles = StyleSheet.create({
  headerContainer: {
    minHeight:100,
    padding:15,
    backgroundColor:'#FFFFFF'
  },
  bodyContainer:{
    minHeight:300,
    padding:15,
    backgroundColor:'#FFFFFF'
  },
  bottomContainer:{
    minHeight:250,
    padding:15,
    backgroundColor:'#FFFFFF'
  },
  HEtextTomada:{
    fontWeight:'bold',
    color:'#ffff',
    fontSize:20,
    paddingTop:10
    // paddingLeft: 50,
  },
  HEtextValorConsumo:{
    color:'#000000',
    fontSize:14,
  },
  containerTable: { padding: 16, paddingTop: 30, backgroundColor: '#fff' },
  head: {  height: 40,  backgroundColor: '#fff'  },
  wrapper: { flexDirection: 'row'},
  title: { backgroundColor: '#fff' },
  row: {  height: 40, backgroundColor:'#9FE3B4' },
  text: { textAlign: 'center' },
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {
    paddingTop: 0,
  },
  contentFlexGraph:{
    paddingTop: 0,
  },
  ScrollViewStyle:{
    marginTop:0,
    maxHeight:285,
    // marginLeft:-25,
    backgroundColor: '#fff',
  },
  ViewTopContent:{
    padding:15,
    marginTop:0,
    marginBottom:10,
    alignContent: 'center',
    alignItems:'center'
  },
  ViewBottomContent:{
    padding:15,
    marginTop:20,
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});
