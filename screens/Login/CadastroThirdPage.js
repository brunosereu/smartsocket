import * as WebBrowser from 'expo-web-browser';
import React, {Component,useState } from 'react';
import {
  Image,
  TextInput,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
  BackHandler,
  Alert
} from 'react-native';

import {
  LineChart,
} from "react-native-chart-kit";
import { Rect, Text as TextSVG, Svg } from "react-native-svg";

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Moment from 'moment';



import FontAWIcon from '../../components/FontAWIcon';
import HeaderComponnent from '../../components/HeaderComponnent';
import ExtratoBlock from '../../components/ExtratoBlock';

class CadastroThirdPage extends Component {

  constructor(props){
    super(props);
    this.scrollRef =  React.createRef()
    this.pinInterval = null
    this.props.navigation.getParam()
    // console.log(this.props.navigation)
    // console.log(this.CustomGraph)
    // this.scrollListReftop.scrollTo({x: 0, y: 0, animated: true})
  }
  state = {
    tooltipPos : { x: 0, y: 0,color:'#E77010', visible: false, value:''}
  };


  backAction = () => {
    return true;
  };

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.backAction);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.backAction);
  }

  customround (number, precision) {
      var factor = Math.pow(10, precision);
      var tempNumber = number * factor;
      var roundedTempNumber = Math.round(tempNumber);
      return roundedTempNumber / factor;
  };



  
  render(){

      this.regua = this.props.regua;
      return (
        <View style={styles.container}>
          <View style={styles.headerContainer}>
                <View style={{  flex:1,flexDirection: 'row' }}>
                      <View style={{flexDirection: 'row', justifyContent:'center',marginTop:20,marginLeft:130}}>
                        <Text style={styles.HEtextValorConsumo}>Cadastro</Text>
                      </View>
                </View>
          </View>
          <View style={styles.bodyContainer}>
                <View style={{  flex:1, }}>
                  <View style={{flexDirection: 'column', justifyContent: 'center'}}>
                    <View style={{flexDirection: 'row', justifyContent:'center',marginBottom:30,marginLeft:0}}>
                        <Text style={{fontSize:20, fontWeight:'bold',}}>Bem Vindo !</Text>
                      </View>
                    <View style={{flexDirection: 'row',justifyContent: 'center',alignItems: 'center'}}>
                        <FontAWIcon color={'#C5E0B4'} name={'check-circle'} size={100}  style={{marginLeft: 0}}/>
                    </View>
                     <View style={{flexDirection: 'row', justifyContent:'center',marginTop:30,marginLeft:0}}>
                        <Text style={{fontSize:14,}}>Obrigado por se cadastrar! Agora falta pouco.</Text>
                    </View>
                    <View style={{flexDirection: 'column', justifyContent:'center',marginTop:30,marginLeft:0}}>
                        <Text style={{fontSize:14,justifyContent:'center',textAlign:'center'}}>Em instantes você receberá um e-mail de confirmação para verificar sua conta</Text>
                        <Text style={{fontSize:14,justifyContent:'center',textAlign:'center'}}>Não se esqueça de checar sua caixa de spam.</Text>
                    </View>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between',padding:20,paddingTop:40,paddingLeft:10}}>
                        <TouchableOpacity onPressIn={()=>{
                            this.props.navigation.navigate('Login')
                        }}> 
                            <View style={{
                                    height:45,
                                    width:310,
                                    backgroundColor:'#E77010',
                                    borderRadius:10,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}>
                            
                                    <Text style={{ fontSize:16,color:'#ffffff',fontWeight:'bold' }}>Finalizar</Text>
                            
                            </View> 
                        </TouchableOpacity>
                    </View>
                  </View>
                </View>
          </View>
    </View>
      );
    }
}

const mapStateToProps = state => ({ regua:state.regua });

export default connect(mapStateToProps,null)(CadastroThirdPage);

CadastroThirdPage.navigationOptions = {
  header: null,
};

const styles = StyleSheet.create({
  headerContainer: {
    minHeight:100,
    padding:15,
    backgroundColor:'#FFFFFF'
  },
  bodyContainer:{
    minHeight:400,
    padding:15,
    backgroundColor:'#FFFFFF'
  },
  bottomContainer:{
    minHeight:250,
    padding:15,
    backgroundColor:'#FFFFFF'
  },
  HEtextTomada:{
    fontWeight:'bold',
    color:'#ffff',
    fontSize:20,
    paddingTop:10
    // paddingLeft: 50,
  },
  HEtextValorConsumo:{
    color:'#000000',
    fontSize:14,
  },
  containerTable: { padding: 16, paddingTop: 30, backgroundColor: '#fff' },
  head: {  height: 40,  backgroundColor: '#fff'  },
  wrapper: { flexDirection: 'row'},
  title: { backgroundColor: '#fff' },
  row: {  height: 40, backgroundColor:'#9FE3B4' },
  text: { textAlign: 'center' },
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {
    paddingTop: 0,
  },
  contentFlexGraph:{
    paddingTop: 0,
  },
  ScrollViewStyle:{
    marginTop:0,
    maxHeight:285,
    // marginLeft:-25,
    backgroundColor: '#fff',
  },
  ViewTopContent:{
    padding:15,
    marginTop:0,
    marginBottom:10,
    alignContent: 'center',
    alignItems:'center'
  },
  ViewBottomContent:{
    padding:15,
    marginTop:20,
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});
