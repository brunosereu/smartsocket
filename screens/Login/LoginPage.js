import * as WebBrowser from 'expo-web-browser';
import React, {Component,useState } from 'react';
import {
  Image,
  TextInput,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions
} from 'react-native';

import {
  LineChart,
} from "react-native-chart-kit";
import { Rect, Text as TextSVG, Svg } from "react-native-svg";
import queryString from 'query-string';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Moment from 'moment';



import FontAWIcon from '../../components/FontAWIcon';
import HeaderComponnent from '../../components/HeaderComponnent';
import ExtratoBlock from '../../components/ExtratoBlock';
import { showMessage, hideMessage } from "react-native-flash-message";
import * as Actions from '../../redux/actions/ReduxActionController';


class LoginPage extends Component {

  constructor(props){
    super(props);
    this.scrollRef =  React.createRef()
    this.pinInterval = null
    this.props.navigation.getParam()
    // console.log(this.props.usuario)
    // console.log(this.CustomGraph)
    // this.scrollListReftop.scrollTo({x: 0, y: 0, animated: true})
  }
  state = {
    tooltipPos : { x: 0, y: 0,color:'#E77010', visible: false, value:''}
  };


  customround (number, precision) {
      var factor = Math.pow(10, precision);
      var tempNumber = number * factor;
      var roundedTempNumber = Math.round(tempNumber);
      return roundedTempNumber / factor;
  };

  onChangeText(value){
    this.setState(value)
  }
  async validateLogin(){
     // console.log(this.state)
     if(this.state.email == null){
      showMessage({
        message: "Verifique",
        description: "Preencha o campo de Email",
        duration:2500,
        type: "default",
        backgroundColor: "#E77010", // background color
        color: "#ffff", // text color
      });
      return false
    }
    let email = this.state.email
    var ee = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);
    if(!ee.test(email) ) { 
      // alert('Preencha o campo email com um e-mail valido'); 
      showMessage({
        message: "Verifique",
        description: "Preencha o campo email com um Email valido",
        duration:2500,
        type: "default",
        backgroundColor: "#E77010", // background color
        color: "#ffff", // text color
      });
      return false; 
    }
    if(this.state.senha == null){
      showMessage({
        message: "Verifique",
        description: "Preencha o campo de senha",
        duration:2500,
        type: "default",
        backgroundColor: "#E77010", // background color
        color: "#ffff", // text color
      });
      return false
    }
    let senha = this.state.senha
    validation = await this.validarOnline(email,senha)
    console.log(validation)
    if(validation.usuario == false){
      showMessage({
        message: "Verifique",
        description: "Não encontramos nenhum usuario com estas credenciais, por favor verifique!",
        duration:2500,
        type: "default",
        backgroundColor: "#E77010", // background color
        color: "#ffff", // text color
      });
      return false
    }
    this.props.Login(Object.assign({},this.props.usuario),{
      email,
      username:validation.usuario.username,
      idUsuario:validation.usuario.idUsuario,
      temRegua:validation.usuario.temRegua,
    }); 
    return false
  }

  async validarOnline(email,senha){
    try{
      let value = await fetch('https://www.brunos4ntos.com/arduino/usuario/verificaUsuario.php', {
          method: 'POST',   
          body: queryString.stringify({
            email:email,
            pswrd:senha
          }),
          headers:{
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        })
        .then((response) => {
          return response.text();
        })
        .then((json) => {
          return json;
        })
        .catch((e) => {
          // console.log(e)
          return {
            status:"error",
            message:"Erro ao Atualizar status da tomada durante a requisição"
          }
        });
        return JSON.parse(value)
      }catch(e){
        return {
          status:"error",
          message:"Não foi possivel enviar sua requisição"
        }
    }
   
  }

  render(){

      this.regua = this.props.regua;
      return (
        <View style={styles.container}>
          <View style={styles.headerContainer}>
                <View style={{  flex:1, }}>
                  <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                        <Image  source={require('../../assets/images/logo.png')} style={{width:100,height:100,marginTop:50}}/>
                  </View>
                </View>
          </View>
          <View style={styles.bodyContainer}>
                <View style={{  flex:1, }}>
                  <View style={{flexDirection: 'column', justifyContent: 'center'}}>
                    <View style={{flexDirection: 'row',justifyContent: 'center',alignItems: 'center',borderWidth: 2,borderColor: '#000',borderRadius: 15, margin: 10,
    height: 40, borderColor: '#707070'}}>
                        <FontAWIcon color={'gray'} name={'user'} size={20}  style={{marginLeft: 20}}/>
                        <TextInput style={{ height: 40,padding:10,width:285 }}
                            onChangeText={(valueText) =>{
                                this.onChangeText({email:valueText})
                            }}
                            value={this.state.text}
                            maxLength={255}
                            placeholder='Email'
                        />
                    </View>
                    <View style={{flexDirection: 'row',justifyContent: 'center',alignItems: 'center',borderWidth: 2,borderColor: '#000',borderRadius: 15, margin: 10,
    height: 40, borderColor: '#707070'}}>
                        <FontAWIcon color={'gray'} name={'lock'} size={20}  style={{marginLeft: 20}}/>
                        <TextInput style={{ height: 40,padding:10,width:285 }}
                            onChangeText={(valueText) =>{
                                this.onChangeText({senha:valueText})
                            }}
                            value={this.state.text}
                            maxLength={20}
                            placeholder='Senha'
                            secureTextEntry={true}
                        />
                    </View>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between',padding:20,paddingTop:0,paddingLeft:10}}>
                        <TouchableOpacity onPressIn={()=>{
                            this.validateLogin();
                        }}> 
                            <View style={{
                                    height:45,
                                    width:310,
                                    backgroundColor:'#C5E0B4',
                                    borderRadius:10,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}>
                            
                                    <Text style={{ fontSize:16,color:'#ffffff',fontWeight:'bold' }}>Entrar</Text>
                            
                            </View> 
                        </TouchableOpacity>
                    </View>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between',padding:20,paddingTop:0,paddingLeft:10}}>
                        <TouchableOpacity onPressIn={()=>{
                            this.props.navigation.navigate('ForgotPasswordPage')
                        }}>
                            <Text>Esqueci a Senha</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPressIn={()=>{
                             this.props.navigation.navigate('CadastroFirstPage')
                        }}>
                            <Text>Cadastre-se</Text>
                        </TouchableOpacity>
                    </View>
                  </View>
                </View>
          </View>
          <View style={styles.bottomContainer}>
                <View style={{  flex:1, }}>
                  <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                        <Image  source={require('../../assets/images/Login.png')} style={{width:300,height:150,marginTop:0}}/>
                  </View>
                </View>
          </View>
    </View>
      );
    }
}

const mapStateToProps = state => ({ regua:state.regua,usuario:state.usuario });

const mapDispatchToProps = dispatch =>  bindActionCreators(Actions, dispatch);


export default connect(mapStateToProps,mapDispatchToProps)(LoginPage);

LoginPage.navigationOptions = {
  header: null,
};

const styles = StyleSheet.create({
  headerContainer: {
    minHeight:200,
    padding:15,
    backgroundColor:'#FFFFFF'
  },
  bodyContainer:{
    minHeight:240,
    padding:15,
    backgroundColor:'#FFFFFF'
  },
  bottomContainer:{
    minHeight:250,
    padding:15,
    backgroundColor:'#FFFFFF'
  },
  HEtextTomada:{
    fontWeight:'bold',
    color:'#ffff',
    fontSize:20,
    paddingTop:10
    // paddingLeft: 50,
  },
  HEtextValorConsumo:{
    color:'#000000',
    fontSize:20,
    fontWeight:'bold',
    paddingTop:10
  },
  containerTable: { padding: 16, paddingTop: 30, backgroundColor: '#fff' },
  head: {  height: 40,  backgroundColor: '#fff'  },
  wrapper: { flexDirection: 'row'},
  title: { backgroundColor: '#fff' },
  row: {  height: 40, backgroundColor:'#9FE3B4' },
  text: { textAlign: 'center' },
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {
    paddingTop: 0,
  },
  contentFlexGraph:{
    paddingTop: 0,
  },
  ScrollViewStyle:{
    marginTop:0,
    maxHeight:285,
    // marginLeft:-25,
    backgroundColor: '#fff',
  },
  ViewTopContent:{
    padding:15,
    marginTop:0,
    marginBottom:10,
    alignContent: 'center',
    alignItems:'center'
  },
  ViewBottomContent:{
    padding:15,
    marginTop:20,
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});
