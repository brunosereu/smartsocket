import * as WebBrowser from 'expo-web-browser';
import React, {Component,useState } from 'react';
import {
  Image,
  TextInput,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions
} from 'react-native';

import {
  LineChart,
} from "react-native-chart-kit";
import { Rect, Text as TextSVG, Svg } from "react-native-svg";
import queryString from 'query-string';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Moment from 'moment';


import { showMessage, hideMessage } from "react-native-flash-message";

import FontAWIcon from '../../components/FontAWIcon';
import HeaderComponnent from '../../components/HeaderComponnent';
import ExtratoBlock from '../../components/ExtratoBlock';

class CadastroSecondPage extends Component {

  constructor(props){
    super(props);
    this.scrollRef =  React.createRef()
    this.pinInterval = null
    this.props.navigation.getParam()
    this.email =  this.props.navigation.getParam('email')
    this.username =  this.props.navigation.getParam('username')

    // console.log(this.props.navigation)
    // console.log(this.CustomGraph)
    // this.scrollListReftop.scrollTo({x: 0, y: 0, animated: true})
  }
  state = {
    senha1:"",
    senha2:"",
  };

  onChangeText(value){
    this.setState(value)
  }

  customround (number, precision) {
      var factor = Math.pow(10, precision);
      var tempNumber = number * factor;
      var roundedTempNumber = Math.round(tempNumber);
      return roundedTempNumber / factor;
  };

  async validaSenha(){
    if(this.state.senha1.length < 4){
      showMessage({
        message: "Verifique",
        description: "Insira uma senha maior que 4 caracteres",
        duration:2500,
        type: "default",
        backgroundColor: "#E77010", // background color
        color: "#ffff", // text color
      });
      return false
    }
    if(this.state.senha2 == "" || this.state.senha2 == null){
      showMessage({
        message: "Verifique",
        description: "Insira sua confirmação de senha",
        duration:2500,
        type: "default",
        backgroundColor: "#E77010", // background color
        color: "#ffff", // text color
      });
      return false
    }
    if(this.state.senha2 != this.state.senha1){
      showMessage({
        message: "Verifique",
        description: "Sua senha e sua confirmação de senha não coincidem",
        duration:2500,
        type: "default",
        backgroundColor: "#E77010", // background color
        color: "#ffff", // text color
      });
      return false
    }
    var password = this.state.senha1
    let idUser = await this.cadastrarOnline(this.email,this.username,password);
    this.props.navigation.navigate('CadastroThirdPage')
  }

  async cadastrarOnline(email,username,senha){
    try{
      let value = await fetch('https://www.brunos4ntos.com/arduino/usuario/adicionarUsuario.php', {
          method: 'POST',   
          body: queryString.stringify({
            email:email,
            username:username,
            pswrd:senha
          }),
          headers:{
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        })
        .then((response) => {
          return response.text();
        })
        .then((json) => {
          return json;
        })
        .catch((e) => {
          // console.log(e)
          return {
            status:"error",
            message:"Erro ao Atualizar status da tomada durante a requisição"
          }
        });
        return JSON.parse(value)
      }catch(e){
        return {
          status:"error",
          message:"Não foi possivel enviar sua requisição"
        }
    }
   
  }
  
  render(){

      this.regua = this.props.regua;
      return (
        <View style={styles.container}>
          <View style={styles.headerContainer}>
                <View style={{  flex:1,flexDirection: 'row' }}>
                      <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop:20}}>
                        <TouchableOpacity   onPress={() => {
                            this.props.navigation.goBack()
                        }}> 
                          <FontAWIcon color={'#000000'} name={'chevron-left'} size={20} style={{marginTop:0}} />
                        </TouchableOpacity>
                      </View>
                      <View style={{flexDirection: 'row', justifyContent:'center',marginTop:20,marginLeft:110}}>
                        <Text style={styles.HEtextValorConsumo}>Cadastro</Text>
                      </View>
                </View>
          </View>
          <View style={styles.bodyContainer}>
                <View style={{  flex:1, }}>
                  <View style={{flexDirection: 'column', justifyContent: 'center'}}>
                    <View style={{flexDirection: 'row', justifyContent:'center',marginBottom:30,marginLeft:0}}>
                        <Text style={{fontSize:20, fontWeight:'bold',}}>Defina sua senha</Text>
                      </View>
                    <View style={{flexDirection: 'row',justifyContent: 'center',alignItems: 'center',borderWidth: 2,borderColor: '#000',borderRadius: 15, margin: 10,
    height: 40, borderColor: '#707070'}}>
                        <FontAWIcon color={'gray'} name={'lock'} size={20}  style={{marginLeft: 20}}/>
                        <TextInput style={{ height: 40,padding:10,width:285,fontSize:12 }}
                            onChangeText={(valueText) =>{
                                this.onChangeText({senha1:valueText})
                            }}
                            value={this.state.senha1}
                            maxLength={20}
                            placeholder='Senha'
                            secureTextEntry={true}
                        />
                    </View>
                    <View style={{flexDirection: 'row',justifyContent: 'center',alignItems: 'center',borderWidth: 2,borderColor: '#000',borderRadius: 15, margin: 10,
    height: 40, borderColor: '#707070'}}>
                        <FontAWIcon color={'gray'} name={'lock'} size={20}  style={{marginLeft: 20}}/>
                        <TextInput style={{ height: 40,padding:10,width:285,fontSize:12 }}
                            onChangeText={(valueText) =>{
                              this.onChangeText({senha2:valueText})
                            }}
                            value={this.state.senha2}
                            maxLength={20}
                            placeholder='Confirme sua senha'
                            secureTextEntry={true}
                        />
                    </View>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between',padding:20,paddingTop:40,paddingLeft:10}}>
                        <TouchableOpacity onPressIn={()=>{
                             this.validaSenha()
                        }}> 
                            <View style={{
                                    height:45,
                                    width:310,
                                    backgroundColor:'#E77010',
                                    borderRadius:10,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}>
                            
                                    <Text style={{ fontSize:16,color:'#ffffff',fontWeight:'bold' }}>Próximo</Text>
                            
                            </View> 
                        </TouchableOpacity>
                    </View>
                  </View>
                </View>
          </View>
    </View>
      );
    }
}

const mapStateToProps = state => ({ regua:state.regua });

export default connect(mapStateToProps,null)(CadastroSecondPage);

CadastroSecondPage.navigationOptions = {
  header: null,
};

const styles = StyleSheet.create({
  headerContainer: {
    minHeight:100,
    padding:15,
    backgroundColor:'#FFFFFF'
  },
  bodyContainer:{
    minHeight:300,
    padding:15,
    backgroundColor:'#FFFFFF'
  },
  bottomContainer:{
    minHeight:250,
    padding:15,
    backgroundColor:'#FFFFFF'
  },
  HEtextTomada:{
    fontWeight:'bold',
    color:'#ffff',
    fontSize:20,
    paddingTop:10
    // paddingLeft: 50,
  },
  HEtextValorConsumo:{
    color:'#000000',
    fontSize:14,
  },
  containerTable: { padding: 16, paddingTop: 30, backgroundColor: '#fff' },
  head: {  height: 40,  backgroundColor: '#fff'  },
  wrapper: { flexDirection: 'row'},
  title: { backgroundColor: '#fff' },
  row: {  height: 40, backgroundColor:'#9FE3B4' },
  text: { textAlign: 'center' },
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {
    paddingTop: 0,
  },
  contentFlexGraph:{
    paddingTop: 0,
  },
  ScrollViewStyle:{
    marginTop:0,
    maxHeight:285,
    // marginLeft:-25,
    backgroundColor: '#fff',
  },
  ViewTopContent:{
    padding:15,
    marginTop:0,
    marginBottom:10,
    alignContent: 'center',
    alignItems:'center'
  },
  ViewBottomContent:{
    padding:15,
    marginTop:20,
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});
