import * as WebBrowser from 'expo-web-browser';
import React, {Component,useState } from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions
} from 'react-native';

import {
  LineChart,
} from "react-native-chart-kit";
import { Rect, Text as TextSVG, Svg } from "react-native-svg";

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Moment from 'moment';



import FontAWIcon from '../components/FontAWIcon';
import HeaderComponnent from '../components/HeaderComponnent';
import ExtratoBlock from '../components/ExtratoBlock';

class GraphScreen extends Component {

  constructor(props){
    super(props);
    this.scrollRef =  React.createRef()
    this.pinInterval = null
    this.props.navigation.getParam()
    // console.log(this.props.navigation)
    // console.log(this.CustomGraph)
    // this.scrollListReftop.scrollTo({x: 0, y: 0, animated: true})
  }
  state = {
    tooltipPos : { x: 0, y: 0,color:'#E77010', visible: false, value:''}
  };


  customround (number, precision) {
      var factor = Math.pow(10, precision);
      var tempNumber = number * factor;
      var roundedTempNumber = Math.round(tempNumber);
      return roundedTempNumber / factor;
  };



  
  render(){
     this.CustomGraph = this.props.navigation.getParam('customGrafico')
     this.regua = this.props.regua;
    //  console.log(this.regua)
     this.listValues = [];
     this.listValues2 = [];

     this.valorTotal = 0;
     this.WattsTotal = 0;
     this.CustomGraph.map(e=>{
      this.WattsTotal += Number(e.WattsT1)+Number(e.WattsT2);
      console.log(Number(e.WattsT1))
      console.log(Number(e.WattsT2))
      this.valorTotal += Number(this.customround(this.customround(0.516*(Number(e.WattsT1)/1000),2)+this.customround(0.516*(Number(e.WattsT2)/1000),2),2).toFixed(2));
     })
     this.CustomGraph.map(e=>{
      // console.log(e)
      this.listValues2.push({
        tomada1Nome:this.regua.tomadas[0].nome,
        tomada2Nome:this.regua.tomadas[1].nome,
        Tomada1KWh:e.RSpKhwT1,
        Tomada1Watts:e.WattsT1,
        Tomada2KWh:e.RSpKhwT2,
        Tomada2Watts:e.WattsT2,
        DataHora: e.DataHora
      })
      this.listValues.push({
        t:1,
        tomada: this.regua.tomadas[0].nome,
        KWh:e.RSpKhwT1,
        Watts:e.WattsT1+' W',
        DataHora: e.DataHora
      })
      this.listValues.push({
        t:2,
        tomada: this.regua.tomadas[1].nome,
        KWh:e.RSpKhwT2,
        Watts:e.WattsT2+' W',
        DataHora: e.DataHora
      })
     })
      return (
        <View style={styles.container}>
          <View style={styles.headerContainer}>
                <View style={{  flex:1, }}>
                  <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                      <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                          <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop:12}}>
                            <TouchableOpacity   onPress={() => {
                                this.props.navigation.goBack()
                            }}> 
                              <FontAWIcon color={'white'} name={'chevron-circle-left'} size={25}  />
                            </TouchableOpacity>
                          </View>
                          <View style={{flexDirection: 'row', justifyContent:'center', marginLeft:50,padding:0}}>
                            <View style={{justifyContent: 'center',alignItems:'center'}}>
                            <Text style={styles.HEtextValorConsumo}>Grafico Customizado</Text>
                            </View>
                          </View>
                      </View>
                  </View>
                </View>
          </View>
          <ScrollView
            style={styles.ScrollViewStyleNormal}
            contentContainerStyle={styles.contentContainer}
            >
              
              <View style={styles.ViewTopContent}>
                  <View> 
                  <Text style={{ fontSize:14,alignSelf:'center',color:'#707070',fontWeight:'bold' }}>Consumo no período</Text> 
                  <Text style={{ fontSize:14,color:'#9FE3B4',fontWeight:'bold' }}>{this.CustomGraph[0].DataHora} - {this.CustomGraph[this.CustomGraph.length-1].DataHora} </Text>
                  </View>
                  <View style={{alignItems:'center'}}>
                        <View style={{
                          width:360,
                          height:100,
                          marginTop:20,
                          borderColor: '#C5E0B4',
                          backgroundColor: '#C5E0B4',
                          borderWidth:2,
                          borderRadius:0,
                        }}
                        >
                          <View style={{
                            width:280,
                            marginLeft:20,
                            marginTop:10,
                            height:70,
                            flexDirection: 'row',
                          justifyContent: 'center'}}>
                                <View  style={{flexDirection: 'column',backgroundColor:'', justifyContent: 'space-between',alignItems:'center',paddingTop:20,paddingLeft:50,paddingBottom:10,paddingRight:90}}>
                                <Text style={{ fontSize:22,fontWeight:'bold',color: '#FFFFFF' }}>{this.WattsTotal.toFixed(2)} W</Text>
                                <Text style={{ fontSize:14,fontWeight:'bold',color: '#FFFFFF' }}>Consumido</Text>
                                </View>
                                <View  style={{flexDirection: 'column', justifyContent: 'space-between',alignItems:'center',paddingTop:20,paddingLeft:15,paddingBottom:20,paddingRight:20}}>
                              <Text style={{ fontSize:22,fontWeight:'bold',color: '#FFFFFF' }}>R$ {this.valorTotal.toFixed(2)}</Text>
                                <Text style={{ fontSize:14,fontWeight:'bold',color: '#FFFFFF' }}>Valor</Text>
                                </View>
                          </View>
                        </View>
                      </View>
              </View>

              
          {
              this.CustomGraph.length != 0 ? 
              <View style={{ border:0, padding:0, borderColor:'black'}}>
              <ScrollView
                style={styles.ScrollViewStyle}
                contentContainerStyle={styles.contentFlexGraph}
                horizontal={true} 
                >
               
                  <LineChart
                    onDataPointClick={(data)=>{
                      // console.log(data.y)
                      // console.log(data.x)
                      if(this.pinInterval != null){
                        clearTimeout(this.pinInterval)
                      }
                      this.setState({
                          tooltipPos:{ 
                            x: data.x, value: (data.getColor() == "#E77010" ? "T1: ":"T2: ") + data.value +"W",color:data.getColor(), y: data.y, visible: true
                          }
                      })
                      this.pinInterval = setTimeout(()=>{
                        this.setState({
                          tooltipPos:{ 
                            x: data.x, value: (data.getColor() == "#E77010" ? "T1: ":"T2: ") + data.value +"W",color:data.getColor(), y: data.y, visible: false
                          }
                        })
                      },3000)

                    }}
                    decorator={()=>{
                        return this.state.tooltipPos.visible ? (<View>
                          <Svg>
                              <Rect x={this.state.tooltipPos.x - 26} 
                                  y={this.state.tooltipPos.y - 20} 
                                  width="50" 
                                  height="15"
                                  style={{borderRadius:2}}
                                  fill="white" />
                                  <TextSVG
                                      x={this.state.tooltipPos.x + 0}
                                      y={this.state.tooltipPos.y - 7}
                                      fill={this.state.tooltipPos.color}
                                      fontSize="11"
                                      fontWeight="bold"
                                      textAnchor="middle">
                                      {this.state.tooltipPos.value}
                                  </TextSVG>
                          </Svg>
                      </View>) : (null)
                    }}
                    withShadow={false}
                    data={{
                      labels: this.CustomGraph.map((e)=>{
                        return Moment(e.DataHora,"DD/MM/YYYY").format('DD/MM')
                      }),
                      datasets: [
                        {
                          data: this.CustomGraph.map((e)=>{
                            return Number(e.WattsT1)
                          }),
                        },
                        {
                          data: this.CustomGraph.map((e)=>{
                            return Number(e.WattsT2)
                          }),
                          color: (opacity = 1) => '#E77010', // optional
                        }
                      ]
                    }}
                    width={550} // from react-native
                    height={180}
                    fromZero={true}
                    yAxisLabel=""
                    yAxisSuffix="W"
                    withHorizontalLabels={true}
                    yAxisInterval={1} // optional, defaults to 1
                    chartConfig={{
                      backgroundGradientFrom: "#ffffff",
                      backgroundGradientTo: "#ffffff",
                      decimalPlaces: 0, // optional, defaults to 2dp
                      color: (opacity = 1) => `#9FE3B4`,
                      labelColor: (opacity = 1) =>'#000000',
                      propsForDots: {
                        r: "6",
                        strokeWidth: "1",
                        stroke: "#FFFFFF"
                      }
                    }}
                    bezier
                    style={{
                      marginVertical: 8,
                      backgroundColor:"#FFFFFF",
                      borderRadius: 0
                    }}
                  />
                  
              </ScrollView>
            
              <View style={styles.ViewBottomContent}>
                <View style={{ flexDirection: 'row'}}>
                  <View style={{ width:15, height:15, backgroundColor:'#E77010', marginTop:0}}>
                  </View>
                  <Text style={{ fontSize:12,color:'#E77010',fontWeight:'bold' }}> {this.props.regua.tomadas[0].nome}</Text> 
                </View>
                <View style={{ flexDirection: 'row'}}>
                  <View style={{ width:15, height:15, backgroundColor:'#9FE3B4', marginTop:0}}>
                  </View>
                  <Text style={{ fontSize:12,color:'#9FE3B4',fontWeight:'bold' }}> {this.props.regua.tomadas[1].nome}</Text> 
                </View>
              </View>
            
            <View style={styles.containerTable}>
              <Text style={{ fontSize:14,alignSelf:'center',color:'#707070',fontWeight:'bold' }}> Extrato detalhado </Text> 
              {
                this.listValues2.map((e,index)=>(
                  this.customround(Number(e.Tomada1Watts)+Number(e.Tomada2Watts),2) != 0 ?

                  <ExtratoBlock key={index} ExtratoBlock= {e}/>
                  :
                  <View key={index}>
                  </View>
                ))
              }
              {/* <Table borderStyle={{borderRadius:10,  borderWidth: 0.2}}>
                <Row data={['Tomada','R$ por Kw/H','Watts','Data']} flexArr={[1, 1, 1, 1]} style={{ height: 50, backgroundColor:'#9FE3B4'}} textStyle={styles.text}/>
                
                  {
                    this.listValues.map((e,index)=>(
                      <Row
                      key={index}
                      flexArr={[1, 1, 1, 1]}
                      data={[e.tomada,e.KWh,e.Watts,e.DataHora]}
                      style={[styles.row, e.t%2 && {backgroundColor: 'white'}]}
                      textStyle={styles.text}
                      />
                    ))
                  }
                  {/* <Rows data={this.listValues.map((e)=>{
                            return [e.tomada,e.KWh,e.Watts,e.DataHora]
                    })} style={[styles.row]} textStyle={styles.text}/>
                </TableWrapper> 
              </Table> */}
            </View>
            </View>
            :
            <View></View>
          }
          </ScrollView>
        </View>
      );
    }
}

const mapStateToProps = state => ({ regua:state.regua });

export default connect(mapStateToProps,null)(GraphScreen);

GraphScreen.navigationOptions = {
  header: null,
};

const styles = StyleSheet.create({
  headerContainer: {
    minHeight:80,
    padding:15,
    backgroundColor:'#C5E0B4'
  },
  HEtextTomada:{
    fontWeight:'bold',
    color:'#ffff',
    fontSize:20,
    paddingTop:10
    // paddingLeft: 50,
  },
  HEtextValorConsumo:{
    color:'#ffff',
    fontSize:20,
    fontWeight:'bold',
    paddingTop:10
  },
  containerTable: { padding: 16, paddingTop: 30, backgroundColor: '#fff' },
  head: {  height: 40,  backgroundColor: '#fff'  },
  wrapper: { flexDirection: 'row'},
  title: { backgroundColor: '#fff' },
  row: {  height: 40, backgroundColor:'#9FE3B4' },
  text: { textAlign: 'center' },
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {
    paddingTop: 0,
  },
  contentFlexGraph:{
    paddingTop: 0,
  },
  ScrollViewStyle:{
    marginTop:0,
    maxHeight:285,
    // marginLeft:-25,
    backgroundColor: '#fff',
  },
  ViewTopContent:{
    padding:15,
    marginTop:0,
    marginBottom:10,
    alignContent: 'center',
    alignItems:'center'
  },
  ViewBottomContent:{
    padding:15,
    marginTop:20,
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});
