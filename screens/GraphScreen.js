import * as WebBrowser from 'expo-web-browser';
import React, {Component,useState } from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions
} from 'react-native';

import {
  LineChart,
} from "react-native-chart-kit";
import { Rect, Text as TextSVG, Svg } from "react-native-svg";

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';


import FontAWIcon from '../components/FontAWIcon';
import HeaderComponnent from '../components/HeaderComponnent';
import CustomModal from '../components/CustomModal';
import ExtratoBlock from '../components/ExtratoBlock';
import SelecionarGraficoCustomizado from '../components/SelecionarGraficoCustomizado';

class GraphScreen extends Component {

  constructor(props){
    super(props);
    this.pinInterval = null
    this.modalGraph =  React.createRef()
  }
  state = {
    tooltipPos : { x: 0, y: 0,color:'#E77010', visible: false, value:''},
  };

  
  
  customround (number, precision) {
      var factor = Math.pow(10, precision);
      var tempNumber = number * factor;
      var roundedTempNumber = Math.round(tempNumber);
      return roundedTempNumber / factor;
  };

  OpenCustomGraph(e){
    this.props.navigation.navigate('CustomGraph',{
      customGrafico:e
    })
  }


  
  render(){
    
     this.regua = this.props.regua;
    //  console.log(this.regua)
     this.listValues = [];
     this.listValues2 = [];
     
     this.regua.dadosGraficoSemanal.map(e=>{
      // console.log(e)
      this.listValues2.push({
        tomada1Nome:this.regua.tomadas[0].nome,
        tomada2Nome:this.regua.tomadas[1].nome,
        Tomada1KWh:e.RSpKhwT1,
        Tomada1Watts:e.WattsT1,
        Tomada2KWh:e.RSpKhwT2,
        Tomada2Watts:e.WattsT2,
        DataHora: e.DataHora
      })
      this.listValues.push({
        t:1,
        tomada: this.regua.tomadas[0].nome,
        KWh:e.RSpKhwT1,
        Watts:e.WattsT1+' W',
        DataHora: e.DataHora
      })
      this.listValues.push({
        t:2,
        tomada: this.regua.tomadas[1].nome,
        KWh:e.RSpKhwT2,
        Watts:e.WattsT2+' W',
        DataHora: e.DataHora
      })
     })
      return (
        <View style={styles.container}>
          <HeaderComponnent />
          <ScrollView
            style={styles.ScrollViewStyleNormal}
            contentContainerStyle={styles.contentContainer}
            >
              <View style={styles.ViewTopContent}>
                <Text style={{ fontSize:15,color:'#9FE3B4',fontWeight:'bold' }}> - Informativo da Semana - </Text> 
                {
                  this.props.regua.dadosGraficoSemanal.length != 0 ? 
                  <View> 
                     <Text style={{ fontSize:12,color:'#9FE3B4',fontWeight:'bold' }}>{this.regua.dadosGraficoSemanal[0].DataHora} - {this.regua.dadosGraficoSemanal[6].DataHora} </Text>   
                  </View>
                  :
                  <View></View>
                }
              </View>
          {
              this.props.regua.dadosGraficoSemanal.length != 0 ? 
              <View style={{ border:0, padding:0, borderColor:'black'}}>
              <ScrollView
                style={styles.ScrollViewStyle}
                contentContainerStyle={styles.contentFlexGraph}
                horizontal={true} 
                >
               
                  <LineChart
                    onDataPointClick={(data)=>{
                      // console.log(data.y)
                      // console.log(data.x)
                      if(this.pinInterval != null){
                        clearTimeout(this.pinInterval)
                      }
                      this.setState({
                          tooltipPos:{ 
                            x: data.x, value: (data.getColor() == "#E77010" ? "T1: ":"T2: ") + data.value +"W",color:data.getColor(), y: data.y, visible: true
                          }
                      })
                      this.pinInterval = setTimeout(()=>{
                        this.setState({
                          tooltipPos:{ 
                            x: data.x, value: (data.getColor() == "#E77010" ? "T1: ":"T2: ") + data.value +"W",color:data.getColor(), y: data.y, visible: false
                          }
                        })
                      },3000)

                    }}
                    decorator={()=>{
                        return this.state.tooltipPos.visible ? (<View>
                          <Svg>
                              <Rect x={this.state.tooltipPos.x - 26} 
                                  y={this.state.tooltipPos.y - 20} 
                                  width="50" 
                                  height="15"
                                  style={{borderRadius:2}}
                                  fill="white" />
                                  <TextSVG
                                      x={this.state.tooltipPos.x + 0}
                                      y={this.state.tooltipPos.y - 7}
                                      fill={this.state.tooltipPos.color}
                                      fontSize="11"
                                      fontWeight="bold"
                                      textAnchor="middle">
                                      {this.state.tooltipPos.value}
                                  </TextSVG>
                          </Svg>
                      </View>) : (null)
                    }}
                    withShadow={false}
                    data={{
                      labels: ["Dom","Seg","Ter", "Qua", "Qui", "Sex", "Sab"],
                      datasets: [
                        {
                          data: this.regua.dadosGraficoSemanal.map((e)=>{
                            return Number(e.WattsT1)
                          }),
                        },
                        {
                          data: this.regua.dadosGraficoSemanal.map((e)=>{
                            return Number(e.WattsT2)
                          }),
                          color: (opacity = 1) => '#E77010', // optional
                        }
                      ]
                    }}
                    width={550} // from react-native
                    height={180}
                    fromZero={true}
                    yAxisLabel=""
                    yAxisSuffix="W"
                    withHorizontalLabels={true}
                    yAxisInterval={1} // optional, defaults to 1
                    chartConfig={{
                      backgroundGradientFrom: "#ffffff",
                      backgroundGradientTo: "#ffffff",
                      decimalPlaces: 0, // optional, defaults to 2dp
                      color: (opacity = 1) => `#9FE3B4`,
                      labelColor: (opacity = 1) =>'#000000',
                      propsForDots: {
                        r: "6",
                        strokeWidth: "1",
                        stroke: "#FFFFFF"
                      }
                    }}
                    bezier
                    style={{
                      marginVertical: 8,
                      backgroundColor:"#FFFFFF",
                      borderRadius: 0
                    }}
                  />
                  
              </ScrollView>
            
              <View style={styles.ViewBottomContent}>
                <View style={{ flexDirection: 'row'}}>
                  <View style={{ width:15, height:15, backgroundColor:'#E77010', marginTop:0}}>
                  </View>
                  <Text style={{ fontSize:12,color:'#E77010',fontWeight:'bold' }}> {this.props.regua.tomadas[0].nome}</Text> 
                </View>
                <View style={{ flexDirection: 'row'}}>
                  <View style={{ width:15, height:15, backgroundColor:'#9FE3B4', marginTop:0}}>
                  </View>
                  <Text style={{ fontSize:12,color:'#9FE3B4',fontWeight:'bold' }}> {this.props.regua.tomadas[1].nome}</Text> 
                </View>
              </View>
           
            <View style={styles.containerTable}>
              <Text style={{ fontSize:14,alignSelf:'center',color:'#707070',fontWeight:'bold' }}> Extrato detalhado </Text> 
              {
                this.listValues2.map((e,index)=>(
                  this.customround(Number(e.Tomada1Watts)+Number(e.Tomada2Watts),2) != 0 ?

                  <ExtratoBlock key={index} ExtratoBlock= {e}/>
                  :
                  <View key={index}>
                  </View>
                ))
              }
              {/* <Table borderStyle={{borderRadius:10,  borderWidth: 0.2}}>
                <Row data={['Tomada','R$ por Kw/H','Watts','Data']} flexArr={[1, 1, 1, 1]} style={{ height: 50, backgroundColor:'#9FE3B4'}} textStyle={styles.text}/>
                
                  {
                    this.listValues.map((e,index)=>(
                      <Row
                      key={index}
                      flexArr={[1, 1, 1, 1]}
                      data={[e.tomada,e.KWh,e.Watts,e.DataHora]}
                      style={[styles.row, e.t%2 && {backgroundColor: 'white'}]}
                      textStyle={styles.text}
                      />
                    ))
                  }
                  {/* <Rows data={this.listValues.map((e)=>{
                            return [e.tomada,e.KWh,e.Watts,e.DataHora]
                    })} style={[styles.row]} textStyle={styles.text}/>
                </TableWrapper> 
              </Table> */}
            </View>
            </View>
            :
            <View></View>
          }
          </ScrollView>
          <View style={{
              height:50,
              width:50,
              backgroundColor:'#C5E0B4',
              borderRadius:50,
              position:'absolute',
              alignItems: 'center',
              justifyContent: 'center',
              borderWidth:1,
              borderColor:'white',
              right: 20,
              top:130,
            }}>
              <TouchableOpacity   onPress={() => {
                    // this.props.navigation.navigate('CustomGraph')
                    this.modalGraph.current.openModal({
                      title: 'Gráfico Customizado',
                      modalVisible: !this.modalGraph.current.state.modalVisible,
                      conteudo:SelecionarGraficoCustomizado
                    });
              }}> 
              <FontAWIcon color={'white'} name={'calendar-day'} size={25}  />
              </TouchableOpacity>
          </View>
          <CustomModal ref={this.modalGraph} props={{OpenCustomGraph:this.OpenCustomGraph.bind(this)}}></CustomModal>
        </View>
      );
    }
}

const mapStateToProps = state => ({ regua:state.regua });

export default connect(mapStateToProps,null)(GraphScreen);

GraphScreen.navigationOptions = {
  header: null,
};

const styles = StyleSheet.create({
  containerTable: { padding: 16, paddingTop: 30, backgroundColor: '#fff' },
  head: {  height: 40,  backgroundColor: '#fff'  },
  wrapper: { flexDirection: 'row'},
  title: { backgroundColor: '#fff' },
  row: {  height: 40, backgroundColor:'#9FE3B4' },
  text: { textAlign: 'center' },
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {
    paddingTop: 0,
  },
  contentFlexGraph:{
    paddingTop: 0,
  },
  ScrollViewStyle:{
    marginTop:0,
    maxHeight:285,
    // marginLeft:-25,
    backgroundColor: '#fff',
  },
  ViewTopContent:{
    padding:15,
    marginTop:0,
    marginBottom:10,
    alignContent: 'center',
    alignItems:'center'
  },
  ViewBottomContent:{
    padding:15,
    marginTop:20,
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});
