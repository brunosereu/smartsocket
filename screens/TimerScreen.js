import * as WebBrowser from 'expo-web-browser';
import React,{Component} from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions
} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import FontAWIcon from '../components/FontAWIcon';
import Timer from '../components/Timer';
import HeaderComponnent from '../components/HeaderComponnent';
import CustomModal from '../components/CustomModal';
import AdicionarTimer from '../components/AdicionarTimer';

class TimerScreen extends Component {
   constructor(props){
    super(props);
    this.modalTimer =  React.createRef()
  }
  render(){
    return (
      <View style={styles.container}>
        <HeaderComponnent />
        <ScrollView
          style={styles.ScrollViewStyle}
          contentContainerStyle={styles.contentContainer}>
            {
              this.props.regua.timers.map((timers,i) =>(
                <Timer key={i} Timer={timers}/>
              ))
            }
            
        </ScrollView>
        <View style={{
              height:50,
              width:50,
              backgroundColor:'#C5E0B4',
              borderRadius:50,
              position:'absolute',
              alignItems: 'center',
              justifyContent: 'center',
              borderWidth:1,
              borderColor:'white',
              right: 20,
              top:130,
            }}>
              <TouchableOpacity   onPress={() => {
                          this.modalTimer.current.openModal({
                            title: 'Adicionar uma Rotina',
                            modalVisible: !this.modalTimer.current.state.modalVisible,
                            conteudo:AdicionarTimer,
                          });
                      }}> 
              <FontAWIcon color={'white'} name={'plus'} size={40}  />
              </TouchableOpacity>
          </View>
          <CustomModal ref={this.modalTimer}></CustomModal>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    maxHeight:Dimensions.get('window').height,
    height:'auto'
  },
  contentContainer: {
    flexDirection: 'column', justifyContent: 'space-between',alignItems:'center'
  },
  ScrollViewStyle:{
    backgroundColor: '#fff',
  }
});


const mapStateToProps = state => ({ regua:state.regua });

export default connect(mapStateToProps,null)(TimerScreen);


