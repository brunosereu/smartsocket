import * as WebBrowser from 'expo-web-browser';
import React, {Component,useState } from 'react';
import {
  Image,
  TextInput,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions
} from 'react-native';
import queryString from 'query-string';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../../redux/actions/ReduxActionController';
import { showMessage, hideMessage } from "react-native-flash-message";
import FontAWIcon from '../../components/FontAWIcon';

class EditAccount extends Component {

  constructor(props){
    super(props);
    this.scrollRef =  React.createRef()
    this.pinInterval = null
    this.props.navigation.getParam()
    // console.log(this.props.navigation)
    // console.log(this.CustomGraph)
    // this.scrollListReftop.scrollTo({x: 0, y: 0, animated: true})
  }
  state = {
    text:"",
    username:""+this.props.usuario.nome,
    email:""+this.props.usuario.email,
    onError:""
 };


  customround (number, precision) {
      var factor = Math.pow(10, precision);
      var tempNumber = number * factor;
      var roundedTempNumber = Math.round(tempNumber);
      return roundedTempNumber / factor;
  };

  onChangeText(value){
    this.setState(value)
  }

  async onTrySubmit(){
    this.setState({onError:""});
    if(this.state.text.length == 0){
      this.setState({onError:"Entre com um ID"});
      return false
    }
    if(this.state.text != "123"){
      setTimeout(function(){ this.setState({onError:"ID não encontrado"}); }.bind(this), 2000);
      return false
    }
    await this.atualizarReguaOnline(this.props.usuario.id);
    this.props.ConfigurarRegua(Object.assign({},this.props.usuario));     
  }

  async atualizarReguaOnline(idUsuario){
    try{
      let value = await fetch('https://www.brunos4ntos.com/arduino/usuario/atualizarUsuario.php', {
          method: 'POST',   
          body: queryString.stringify({
            idUsuario:idUsuario,
            temRegua:1
          }),
          headers:{
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        })
        .then((response) => {
          return response.text();
        })
        .then((json) => {
          return json;
        })
        .catch((e) => {
          // console.log(e)
          return {
            status:"error",
            message:"Erro ao Atualizar status da tomada durante a requisição"
          }
        });
        return JSON.parse(value)
      }catch(e){
        return {
          status:"error",
          message:"Não foi possivel enviar sua requisição"
        }
    }
  }

  logout(){
    this.props.Logout(Object.assign({},this.props.usuario),{});     
  }

  
  render(){

      this.regua = this.props.regua;
      return (
        <View style={styles.container}>
          <View style={styles.headerContainer}>
                <View style={{  flex:1,flexDirection: 'row' }}>
                      <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop:20}}>
                        <TouchableOpacity   onPress={() => {
                            this.props.navigation.goBack()
                        }}> 
                          <FontAWIcon color={'#000000'} name={'chevron-left'} size={20} style={{marginTop:0}} />
                        </TouchableOpacity>
                      </View>
                      <View style={{flexDirection: 'row', justifyContent:'center',marginTop:20,marginLeft:100}}>
                        <Text style={styles.HEtextValorConsumo}>Adicionar Régua</Text>
                      </View>
                </View>
          </View>
          <View style={styles.bodyContainer}>
               
                <View style={{padding:20}}>
                <Text style={{ marginLeft:8,marginBottom:25,fontSize:14,fontWeight:'bold' }} >ID da Regua</Text>
                <TextInput
                  style={{ height: 40, borderColor: 'gray', borderWidth:2,
                  borderRadius:15,padding:10 }}
                  onChangeText={(valueText) =>{
                    this.onChangeText({text:valueText})
                  }}
                  value={this.state.text}
                  maxLength={20}
                />
                <Text style={{ marginLeft:8,marginBottom:25,fontSize:12,color:'#E77010',fontWeight:'bold' }} >{this.state.onError}</Text>
              </View>
                
                <View style={{flexDirection: 'row-reverse', justifyContent: 'center',padding:10}}>
                    
                    <View style={{
                          height:45,
                          width:300,
                          backgroundColor:'#C5E0B4',
                          borderRadius:10,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <TouchableOpacity onPressIn={()=>{
                          this.onTrySubmit(false)
                        }}> 
                                <Text style={{ fontSize:16,color:'#ffffff',fontWeight:'bold' }}>Adicionar</Text>
                        </TouchableOpacity>
                    </View> 
                </View>
          </View>
    </View>
      );
    }
}

const mapStateToProps = state => ({ regua:state.regua,usuario: state.usuario });
const mapDispatchToProps = dispatch =>  bindActionCreators(Actions, dispatch);


export default connect(mapStateToProps,mapDispatchToProps)(EditAccount);

EditAccount.navigationOptions = {
  header: null,
};

const styles = StyleSheet.create({
  headerContainer: {
    minHeight:80,
    padding:15,
    backgroundColor:'#FFFFFF'
  },
  bodyContainer:{
    minHeight:300,
    padding:15,
    paddingTop:0,
    backgroundColor:'#FFFFFF'
  },
  bottomContainer:{
    minHeight:250,
    padding:15,
    backgroundColor:'#FFFFFF'
  },
  HEtextTomada:{
    fontWeight:'bold',
    color:'#ffff',
    fontSize:20,
    paddingTop:10
    // paddingLeft: 50,
  },
  HEtextValorConsumo:{
    color:'#000000',
    fontSize:11,
  },
  containerTable: { padding: 16, paddingTop: 30, backgroundColor: '#fff' },
  head: {  height: 40,  backgroundColor: '#fff'  },
  wrapper: { flexDirection: 'row'},
  title: { backgroundColor: '#fff' },
  row: {  height: 40, backgroundColor:'#9FE3B4' },
  text: { textAlign: 'center' },
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {
    paddingTop: 0,
  },
  contentFlexGraph:{
    paddingTop: 0,
  },
  ScrollViewStyle:{
    marginTop:0,
    maxHeight:285,
    // marginLeft:-25,
    backgroundColor: '#fff',
  },
  ViewTopContent:{
    padding:15,
    marginTop:0,
    marginBottom:10,
    alignContent: 'center',
    alignItems:'center'
  },
  ViewBottomContent:{
    padding:15,
    marginTop:20,
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});
