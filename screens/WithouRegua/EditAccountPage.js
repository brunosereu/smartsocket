import * as WebBrowser from 'expo-web-browser';
import React, {Component,useState } from 'react';
import {
  Image,
  TextInput,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions
} from 'react-native';
import queryString from 'query-string';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../../redux/actions/ReduxActionController';
import { showMessage, hideMessage } from "react-native-flash-message";
import FontAWIcon from '../../components/FontAWIcon';

class EditAccount extends Component {

  constructor(props){
    super(props);
    this.scrollRef =  React.createRef()
    this.pinInterval = null
    this.props.navigation.getParam()
    console.log(this.props.usuario)
    // console.log(this.CustomGraph)
    // this.scrollListReftop.scrollTo({x: 0, y: 0, animated: true})
  }
  state = {
    text:""+this.props.regua.nome,
    username:""+this.props.usuario.nome,
    email:""+this.props.usuario.email,
    onError:""
 };


  customround (number, precision) {
      var factor = Math.pow(10, precision);
      var tempNumber = number * factor;
      var roundedTempNumber = Math.round(tempNumber);
      return roundedTempNumber / factor;
  };

  async onTrySubmit(){
    if(this.state.email == null){
      showMessage({
        message: "Verifique",
        description: "Preencha o campo de email",
        duration:2500,
        type: "default",
        backgroundColor: "#E77010", // background color
        color: "#ffff", // text color
      });
      return false
    }
    let email = this.state.email
    var ee = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);
    if(!ee.test(email) ) { 
      // alert('Preencha o campo email com um e-mail valido'); 
      showMessage({
        message: "Verifique",
        description: "Preencha o campo email com um e-mail valido",
        duration:2500,
        type: "default",
        backgroundColor: "#E77010", // background color
        color: "#ffff", // text color
      });
      return false; 
    }
    if(this.state.username == null){
      showMessage({
        message: "Verifique",
        description: "Preencha o campo de nome de usuário",
        duration:2500,
        type: "default",
        backgroundColor: "#E77010", // background color
        color: "#ffff", // text color
      });
      return false
    }
    var en = new RegExp(/^[A-Za-z0-9]*$/);
    let username = this.state.username
    if(!en.test(username) ) { 
      // alert('Preencha o campo email com um e-mail valido'); 
      showMessage({
        message: "Verifique",
        description: "Preencha o campo de nome de usuário com apenas números ou letras",
        duration:2500,
        type: "default",
        backgroundColor: "#E77010", // background color
        color: "#ffff", // text color
      });
      return false; 
    }
    let password = ""
    if(this.state.senha1 != null && this.state.senha1 != "" ){
      if(this.state.senha1.length < 4){
        showMessage({
          message: "Verifique",
          description: "Insira uma senha maior que 4 caracteres",
          duration:2500,
          type: "default",
          backgroundColor: "#E77010", // background color
          color: "#ffff", // text color
        });
        return false
      }
      if(this.state.senha2 == "" || this.state.senha2 == null){
        showMessage({
          message: "Verifique",
          description: "Insira sua confirmação de senha",
          duration:2500,
          type: "default",
          backgroundColor: "#E77010", // background color
          color: "#ffff", // text color
        });
        return false
      }
      if(this.state.senha2 != this.state.senha1){
        showMessage({
          message: "Verifique",
          description: "Sua senha e sua confirmação de senha não coincidem",
          duration:2500,
          type: "default",
          backgroundColor: "#E77010", // background color
          color: "#ffff", // text color
        });
        return false
      }
      password = this.state.senha1
    }
     
    
    let returned = await this.editarOnline({
      idUsuario: this.props.usuario.id,
      email,
      username,
      password});

    this.props.EditUser(Object.assign({},this.props.usuario),{email,username});    
    this.props.navigation.goBack() 
  }

  async editarOnline(usuario){
    try{
      let value = await fetch('https://www.brunos4ntos.com/arduino/usuario/editarUsuario.php', {
          method: 'POST',   
          body: queryString.stringify({
            idUsuario:usuario.idUsuario,
            email:usuario.email,
            username:usuario.username,
            pswrd:usuario.password
          }),
          headers:{
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        })
        .then((response) => {
          return response.text();
        })
        .then((json) => {
          return json;
        })
        .catch((e) => {
          // console.log(e)
          return {
            status:"error",
            message:"Erro ao Atualizar status da tomada durante a requisição"
          }
        });
        return JSON.parse(value)
      }catch(e){
        return {
          status:"error",
          message:"Não foi possivel enviar sua requisição"
        }
    }
   
  }

  onChangeText(value){
    this.setState(value)
  }

  logout(){
    this.props.Logout(Object.assign({},this.props.usuario),{});     
  }

  
  render(){

      this.regua = this.props.regua;
      return (
        <View style={styles.container}>
          <View style={styles.headerContainer}>
                <View style={{  flex:1,flexDirection: 'row' }}>
                      <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop:20}}>
                        <TouchableOpacity   onPress={() => {
                            this.props.navigation.goBack()
                        }}> 
                          <FontAWIcon color={'#000000'} name={'chevron-left'} size={20} style={{marginTop:0}} />
                        </TouchableOpacity>
                      </View>
                      <View style={{flexDirection: 'row', justifyContent:'center',marginTop:20,marginLeft:90}}>
                        <Text style={styles.HEtextValorConsumo}>Configurar Conta</Text>
                      </View>
                </View>
          </View>
          <View style={styles.bodyContainer}>
                <View style={{flexDirection: 'row-reverse', justifyContent: 'space-between',padding:10}}>
                        <View style={{
                          height:30,
                          width:70,
                          backgroundColor:'#E77010',
                          borderRadius:10,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <TouchableOpacity onPressIn={()=>{
                          this.logout()
                        }}> 
                                <Text style={{ fontSize:12,color:'#ffffff',fontWeight:'bold' }}>Sair</Text>
                        </TouchableOpacity>
                    </View> 
                </View>
                <View style={{flexDirection: 'row-reverse', justifyContent: 'center',padding:10}}>
                        <FontAWIcon color={'grey'} name={'user-circle'} size={75} style={{marginTop:0}} />
                </View>
               
                <View style={{padding:10,paddingTop:3,paddingBottom:3}}>
                  <Text style={{ marginLeft:8,marginBottom:5,fontSize:11,fontWeight:'bold' }} >Email</Text>
                  <TextInput
                    style={{ height: 40, borderColor: 'gray', borderWidth:2,
                    borderRadius:15,padding:10,fontSize:12 }}
                    onChangeText={(valueText) =>{
                      this.onChangeText({email:valueText})
                    }}
                    value={this.state.email}
                    maxLength={255}
                  />
                  <Text style={{ marginLeft:8,marginBottom:5,fontSize:12,color:'#E77010',fontWeight:'bold' }} >{this.state.onError}</Text>
                </View>
                <View style={{padding:10,paddingBottom:3}}>
                  <Text style={{ marginLeft:8,marginBottom:5,fontSize:11,fontWeight:'bold' }} >Nome de Usuario</Text>
                  <TextInput
                    style={{ height: 40, borderColor: 'gray', borderWidth:2,
                    borderRadius:15,padding:10,fontSize:12 }}
                    onChangeText={(valueText) =>{
                      this.onChangeText({username:valueText})
                    }}
                    value={this.state.username}
                    maxLength={20}
                    placeholder='(Apenas números ou letras)'
                  />
                  <Text style={{ marginLeft:8,marginBottom:5,fontSize:12,color:'#E77010',fontWeight:'bold' }} >{this.state.onError}</Text>
                </View>
                <View style={{padding:10,paddingBottom:3,paddingTop:3}}>
                <Text style={{ marginLeft:8,marginBottom:5,fontSize:11, }} >Caso deseje alterar a senha:</Text>
                </View>

                <View style={{padding:10,paddingTop:3,paddingBottom:3}}>
                  <Text style={{ marginLeft:8,marginBottom:5,fontSize:11,fontWeight:'bold' }} >Senha</Text>
                  <TextInput
                    style={{ height: 40, borderColor: 'gray', borderWidth:2,
                    borderRadius:15,padding:10,fontSize:12}}
                    onChangeText={(valueText) =>{
                      this.onChangeText({senha1:valueText})
                    }}
                    maxLength={20}
                    secureTextEntry={true}
                  />
                  <Text style={{ marginLeft:8,marginBottom:5,fontSize:12,color:'#E77010',fontWeight:'bold' }} >{this.state.onError}</Text>
                </View>
                <View style={{padding:10,paddingTop:3,paddingBottom:3}}>
                  <Text style={{ marginLeft:8,marginBottom:5,fontSize:11,fontWeight:'bold' }} >Confirme sua senha</Text>
                  <TextInput
                    style={{ height: 40, borderColor: 'gray', borderWidth:2,
                    borderRadius:15,padding:10,fontSize:12 }}
                    onChangeText={(valueText) =>{
                      this.onChangeText({senha2:valueText})
                    }}
                    maxLength={20}
                    secureTextEntry={true}
                  />
                  <Text style={{ marginLeft:8,marginBottom:5,fontSize:12,color:'#E77010',fontWeight:'bold' }} >{this.state.onError}</Text>
                </View>
                <View style={{flexDirection: 'row-reverse', justifyContent: 'center',padding:10}}>
                    
                    <View style={{
                          height:45,
                          width:300,
                          backgroundColor:'#C5E0B4',
                          borderRadius:10,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <TouchableOpacity onPressIn={()=>{
                          this.onTrySubmit(false)
                        }}> 
                                <Text style={{ fontSize:16,color:'#ffffff',fontWeight:'bold' }}>Atualizar</Text>
                        </TouchableOpacity>
                    </View> 
                </View>
          </View>
    </View>
      );
    }
}

const mapStateToProps = state => ({ regua:state.regua,usuario: state.usuario });
const mapDispatchToProps = dispatch =>  bindActionCreators(Actions, dispatch);


export default connect(mapStateToProps,mapDispatchToProps)(EditAccount);

EditAccount.navigationOptions = {
  header: null,
};

const styles = StyleSheet.create({
  headerContainer: {
    minHeight:80,
    padding:15,
    backgroundColor:'#FFFFFF'
  },
  bodyContainer:{
    minHeight:300,
    padding:15,
    paddingTop:0,
    backgroundColor:'#FFFFFF'
  },
  bottomContainer:{
    minHeight:250,
    padding:15,
    backgroundColor:'#FFFFFF'
  },
  HEtextTomada:{
    fontWeight:'bold',
    color:'#ffff',
    fontSize:20,
    paddingTop:10
    // paddingLeft: 50,
  },
  HEtextValorConsumo:{
    color:'#000000',
    fontSize:11,
  },
  containerTable: { padding: 16, paddingTop: 30, backgroundColor: '#fff' },
  head: {  height: 40,  backgroundColor: '#fff'  },
  wrapper: { flexDirection: 'row'},
  title: { backgroundColor: '#fff' },
  row: {  height: 40, backgroundColor:'#9FE3B4' },
  text: { textAlign: 'center' },
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {
    paddingTop: 0,
  },
  contentFlexGraph:{
    paddingTop: 0,
  },
  ScrollViewStyle:{
    marginTop:0,
    maxHeight:285,
    // marginLeft:-25,
    backgroundColor: '#fff',
  },
  ViewTopContent:{
    padding:15,
    marginTop:0,
    marginBottom:10,
    alignContent: 'center',
    alignItems:'center'
  },
  ViewBottomContent:{
    padding:15,
    marginTop:20,
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});
