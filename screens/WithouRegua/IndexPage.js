import * as WebBrowser from 'expo-web-browser';
import React, {Component,useState } from 'react';
import {
  Image,
  TextInput,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions
} from 'react-native';

import {
  LineChart,
} from "react-native-chart-kit";
import { Rect, Text as TextSVG, Svg } from "react-native-svg";

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Moment from 'moment';



import FontAWIcon from '../../components/FontAWIcon';
import HeaderComponnent from '../../components/HeaderComponnent';
import ExtratoBlock from '../../components/ExtratoBlock';
import { showMessage, hideMessage } from "react-native-flash-message";
import * as Actions from '../../redux/actions/ReduxActionController';


class IndexPage extends Component {

  constructor(props){
    super(props);
    this.scrollRef =  React.createRef()
    this.pinInterval = null
    this.props.navigation.getParam()
    // console.log(this.props.usuario)
    // console.log(this.CustomGraph)
    // this.scrollListReftop.scrollTo({x: 0, y: 0, animated: true})
  }
  state = {
    tooltipPos : { x: 0, y: 0,color:'#E77010', visible: false, value:''}
  };




  onChangeText(value){
    this.setState(value)
  }
 
  
  render(){

      this.regua = this.props.regua;
      return (
        <View style={styles.container}>
          <View style={styles.headerContainer}>
                <View style={{  flex:1, }}>
                  <View style={{flexDirection: 'row',paddingTop:30}}>
                        <FontAWIcon color={'gray'} name={'user-circle'} size={50}  style={{marginLeft: 20,marginRight:20,marginTop:0}}/>
                        <Text style={{fontSize:18,marginTop:10}}>{this.props.usuario.nome}</Text>
                  </View>
                </View>
          </View>
          <View style={styles.bodyContainer}>
                <View style={{  flex:1, }}>
                  <View style={{flexDirection: 'column', justifyContent: 'center'}}>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between',padding:20,paddingTop:0,paddingLeft:10}}>
                        <TouchableOpacity onPressIn={()=>{
                            this.props.navigation.navigate('AdicionarReguaPage')
                        }}> 
                            <View style={{
                                    height:60,
                                    width:310,
                                    backgroundColor:'#C5E0B4',
                                    borderRadius:10,
                                    paddingLeft:20,
                                    paddingTop:20,
                                    // justifyContent: 'center',
                                    flexDirection:'row'
                                }}>
                                    <FontAWIcon color={'white'} name={'plug'} size={18}  style={{marginLeft: 0,marginRight:20,marginTop:2}}/>
                                    <Text style={{ fontSize:16,color:'#ffffff',fontWeight:'bold' }}>Adicionar nova Régua</Text>
                            </View> 
                        </TouchableOpacity>
                    </View>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between',padding:20,paddingTop:0,paddingLeft:10}}>
                        <TouchableOpacity onPressIn={()=>{
                            this.props.navigation.navigate('EditAccountPage')
                        }}> 
                            <View style={{
                                    height:60,
                                    width:310,
                                    backgroundColor:'#E77010',
                                    borderRadius:10,
                                    paddingLeft:20,
                                    paddingTop:20,
                                    // justifyContent: 'center',
                                    flexDirection:'row'
                                }}>
                                    <FontAWIcon color={'white'} name={'tools'} size={18}  style={{marginLeft: 0,marginRight:20,marginTop:2}}/>
                                    <Text style={{ fontSize:16,color:'#ffffff',fontWeight:'bold' }}>Configurar Conta</Text>
                            </View> 
                        </TouchableOpacity>
                    </View>
                  </View>
                </View>
          </View>
    </View>
      );
    }
}

const mapStateToProps = state => ({ regua:state.regua,usuario:state.usuario });

const mapDispatchToProps = dispatch =>  bindActionCreators(Actions, dispatch);


export default connect(mapStateToProps,mapDispatchToProps)(IndexPage);

IndexPage.navigationOptions = {
  header: null,
};

const styles = StyleSheet.create({
  headerContainer: {
    minHeight:175,
    padding:15,
    backgroundColor:'#FFFFFF'
  },
  bodyContainer:{
    minHeight:240,
    padding:15,
    backgroundColor:'#FFFFFF'
  },
  bottomContainer:{
    minHeight:250,
    padding:15,
    backgroundColor:'#FFFFFF'
  },
  HEtextTomada:{
    fontWeight:'bold',
    color:'#ffff',
    fontSize:20,
    paddingTop:10
    // paddingLeft: 50,
  },
  HEtextValorConsumo:{
    color:'#000000',
    fontSize:20,
    fontWeight:'bold',
    paddingTop:10
  },
  containerTable: { padding: 16, paddingTop: 30, backgroundColor: '#fff' },
  head: {  height: 40,  backgroundColor: '#fff'  },
  wrapper: { flexDirection: 'row'},
  title: { backgroundColor: '#fff' },
  row: {  height: 40, backgroundColor:'#9FE3B4' },
  text: { textAlign: 'center' },
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {
    paddingTop: 0,
  },
  contentFlexGraph:{
    paddingTop: 0,
  },
  ScrollViewStyle:{
    marginTop:0,
    maxHeight:285,
    // marginLeft:-25,
    backgroundColor: '#fff',
  },
  ViewTopContent:{
    padding:15,
    marginTop:0,
    marginBottom:10,
    alignContent: 'center',
    alignItems:'center'
  },
  ViewBottomContent:{
    padding:15,
    marginTop:20,
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});
