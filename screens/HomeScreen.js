import * as WebBrowser from 'expo-web-browser';
import React, {Component} from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
  Modal
} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../redux/actions/ReduxActionController';

import FontAWIcon from '../components/FontAWIcon';
import Tomada from '../components/Tomada';
import HeaderComponnent from '../components/HeaderComponnent';
import { showMessage, hideMessage } from "react-native-flash-message";



class HomeScreen extends Component {

  constructor(props){
    super(props);
    // console.log(this.props.usuario)
  }
  
  async componentDidMount(){
      let lastStatusTomada = await this.updateLastStatusTomada();
      let lastGraph = await this.updateLastGraph();
      let lastMounth = await this.updateMounthConsumo();
      showMessage({
        message: "Atualizando dados das tomadas",
        description: "",
        duration:2500,
        type: "default",
        backgroundColor: "#9FE3B4", // background color
        color: "#ffff", // text color
      });
      if(lastStatusTomada.status=="ok" && lastGraph.status == "ok" && lastMounth.status == "ok"){
        this.props.UpdateStatusOnlineTomada(Object.assign({},this.props.regua),lastStatusTomada.json);   
        this.props.UpdateValuesGraficoSemanal(Object.assign({},this.props.regua),lastGraph);    
        this.props.UpdateValues(Object.assign({},this.props.regua),lastMounth);   
      }else{
          showMessage({
              message: "Ocorreu um Erro",
              description: "Não foi possivel atualizar os dados automaticamente",
              duration:2500,
              type: "default",
              backgroundColor: "#E77010", // background color
              color: "#ffff", // text color
          });
      }
  }

  async updateLastGraph(){
    try{
      let value = await fetch('http://brunos4ntos.com/arduino/graficoDaSemanaDeConsumo.php', {
        method: 'GET',   
        headers:{
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      })
      .then((response) => {
        return response.text();
      })
      .then((json) => {
        // console.log(JSON.parse(json));
        return {
          status:'ok',
          json:JSON.parse(json)
        };
      })
      .catch((error) => {
        return {
          status:"error",
          message:"Erro ao retornar dados"
        }
      });
      return value
    }catch(e){
      return {
        status:"error",
        message:"Não foi possivel enviar sua requisição"
      }
    }
  }


  async updateLastStatusTomada(){
    try{
      let value = await fetch('http://brunos4ntos.com/arduino/statusTomada.php', {
        method: 'GET',   
        headers:{
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      })
      .then((response) => {
        return response.text();
      })
      .then((json) => {
        // console.log(JSON.parse(json));
        return {
          status:'ok',
          json:JSON.parse(json)
        };
      })
      .catch((error) => {
        return {
          status:"error",
          message:"Erro ao retornar dados"
        }
      });
      return value
    }catch(e){
      return {
        status:"error",
        message:"Não foi possivel enviar sua requisição"
      }
    }
  }

  async updateMounthConsumo(){
    try{
      let value = await fetch('http://brunos4ntos.com/arduino/DadosDoMesAtual.php', {
        method: 'GET',   
        headers:{
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      })
      .then((response) => {
        return response.text();
      })
      .then((json) => {
        // console.log(JSON.parse(json));
        return {
          status:'ok',
          json:JSON.parse(json)
        };
      })
      .catch((error) => {
        return {
          status:"error",
          message:"Erro ao retornar dados"
        }
      });
      return value
    }catch(e){
      return {
        status:"error",
        message:"Não foi possivel enviar sua requisição"
      }
    }
  }


  render(){
    this.regua = this.props.regua;
    return (
      <View style={styles.container}>
       <HeaderComponnent/>
         <ScrollView
           style={styles.ScrollViewStyle}
           contentContainerStyle={styles.contentContainer}>
             {
               this.props.regua.tomadas.map((tomada,i) =>(
                 <Tomada key={i} Tomada={tomada}/>
              ))
            }

         </ScrollView>
        
      </View>
    );
  }
  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    maxHeight:Dimensions.get('window').height,
    height:'auto'
  },
  contentContainer: {
    flexDirection: 'column', justifyContent: 'space-between',alignItems:'center'
  },
  ScrollViewStyle:{
    backgroundColor: '#fff',
  },
 
});


const mapStateToProps = state => ({ regua:state.regua,usuario:state.usuario });

const mapDispatchToProps = dispatch =>  bindActionCreators(Actions, dispatch);

export default connect(mapStateToProps,mapDispatchToProps)(HomeScreen);


