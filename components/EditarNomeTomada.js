import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
    Dimensions,
    Modal,
    ScrollView,
    TouchableWithoutFeedback
  } from 'react-native';
import { AsyncStorage } from 'react-native';
import FontAWIcon from './FontAWIcon';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as Actions from '../redux/actions/ReduxActionController';

class EditarNomeTomada extends Component {
    
    constructor(props) {
        super(props);
        tomada = this.props.props.props.tomada;
        
    }
    state = {
      text:""+this.props.props.props.tomada.nome,
      onError:""
   };
   
    onChangeText(text){
      this.setState((previousState)=>{
        return {
          text:text
        }
      })
    }

    onTrySubmit(){
      if(this.state.text.length < 5){
        this.setState({onError:"Insira no minimo 5 caracteres"})
        return;
      }
      this.tomada.nome = this.state.text;
      this.props.UpdateNomeTomada(Object.assign({},this.props.regua),this.tomada);     
      this.props.setModalInvisible(false)
    }
    render() {
        
        this.tomada = this.props.props.props.tomada;
        return (
            <View>
              <View style={{padding:20}}>
                <Text style={{ marginLeft:8,marginBottom:25,fontSize:14,fontWeight:'bold' }} >Apelido da Tomada</Text>
                <TextInput
                  style={{ height: 40, borderColor: 'gray', borderWidth:2,
                  borderRadius:15,padding:10 }}
                  onChangeText={(valueText) =>{
                    this.onChangeText(valueText)
                  }}
                  value={this.state.text}
                  maxLength={12}
                />
                <Text style={{ marginLeft:8,marginBottom:25,fontSize:12,color:'#E77010',fontWeight:'bold' }} >{this.state.onError}</Text>
              </View>
              <View style={{flexDirection: 'row-reverse', justifyContent: 'space-between',padding:20}}>
                  <View style={{
                        height:55,
                        width:95,
                        backgroundColor:'#C5E0B4',
                        borderRadius:30,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <TouchableOpacity onPressIn={()=>{
                        this.onTrySubmit(false)
                      }}> 
                              <Text style={{ fontSize:16,color:'#ffffff',fontWeight:'bold' }}>Atualizar</Text>
                      </TouchableOpacity>
                  </View> 
              </View>
            </View>
        );
    }
}


const mapStateToProps = state => ({ regua:state.regua });

const mapDispatchToProps = dispatch =>  bindActionCreators(Actions, dispatch);


export default connect(mapStateToProps,mapDispatchToProps)(EditarNomeTomada)

const styles = StyleSheet.create({
    containerModal:{
        flex: 1,
        backgroundColor:'#eff0f1c9',
        maxHeight:Dimensions.get('window').height,
        height:'auto'
    },
    
});
  