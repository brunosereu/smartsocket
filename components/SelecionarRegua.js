import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
    Dimensions,
    Modal,
    ScrollView,
    TouchableWithoutFeedback
  } from 'react-native';
import { AsyncStorage } from 'react-native';
import FontAWIcon from './FontAWIcon';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import CustomModal from './CustomModal';
import AdicionarRegua from './AdicionarRegua';


import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as Actions from '../redux/actions/ReduxActionController';

class SelecionarRegua extends Component {
    
    constructor(props) {
        super(props);
        // console.log(props)
        regua = this.props.regua;
        this.modalAdicionarRegua =  React.createRef()
        
    }
    state = {
      text:""+this.props.regua.nome,
      onError:""
   };
   
    onChangeText(text){
      this.setState((previousState)=>{
        return {
          text:text
        }
      })
    }

    onTrySubmit(){
      if(this.state.text.length < 5){
        this.setState({onError:"Insira no minimo 5 caracteres"})
        return;
      }
      this.props.UpdateNomeRegua(Object.assign({},this.props.regua),this.state.text);     
      this.props.setModalInvisible(false)
    }
    render() {
      this.regua = this.props.regua;
        var radio_props = [
          {label: this.regua.nome, value: 1 },
        ];
        return (
            <View>
              <View style={{padding:20,marginBottom:70}}>
                <RadioForm
                  radio_props={radio_props}
                  initial={0}
                  buttonColor={'#707070'}
                  selectedButtonColor={'#707070'}
                  style={{padding:5}}
                  formHorizontal={true}
                  onPress={(value) => {
                    // console.log(value)
                  }}
                />
              </View>
              <View style={{flexDirection: 'row', justifyContent: 'space-between',padding:20}}>
                  <View style={{
                        height:45,
                        width:180,
                        backgroundColor:'#C5E0B4',
                        borderRadius:30,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <TouchableOpacity onPressIn={()=>{
                        this.modalAdicionarRegua.current.openModal({
                          title: 'Adicionar uma Régua',
                          modalVisible: !this.modalAdicionarRegua.current.state.modalVisible,
                          conteudo:AdicionarRegua,
                        });
                      }} style={{flexDirection:"row"}}> 
                              <Text style={{ fontSize:15,color:'#ffffff',fontWeight:'bold' }}>Adicione uma Régua</Text>
                              <FontAWIcon color={'white'} name={'plus'} size={15} style={{marginTop:3,marginLeft:5}} />
                      </TouchableOpacity>
                  </View> 
              </View>
              <CustomModal ref={this.modalAdicionarRegua}></CustomModal>
            </View>
        );
    }
}


const mapStateToProps = state => ({ regua:state.regua });

const mapDispatchToProps = dispatch =>  bindActionCreators(Actions, dispatch);


export default connect(mapStateToProps,mapDispatchToProps)(SelecionarRegua)

const styles = StyleSheet.create({
    containerModal:{
        flex: 1,
        backgroundColor:'#eff0f1c9',
        maxHeight:Dimensions.get('window').height,
        height:'auto'
    },
    
});
  