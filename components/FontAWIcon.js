import React from 'react';
import { View, Text } from 'react-native'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faChartBar,faEllipsisV,faChevronDown,faArrowLeft,faHome,faStopwatch,faSyncAlt,
  faChartLine,faPlus,faCalendarDay,faChevronCircleLeft,faUser,faLock,faChevronLeft,faEnvelope,faCheckCircle,faUserCircle,faTools, faPlug } from '@fortawesome/free-solid-svg-icons'

library.add(faChartBar,faEllipsisV,faChevronDown,faArrowLeft,faHome,
  faStopwatch,faSyncAlt,faChartLine,faPlus,faCalendarDay,faChevronCircleLeft,faUser,faLock,faChevronLeft,faEnvelope,faCheckCircle,faUserCircle,faTools,faPlug)

import Colors from '../constants/Colors';


export default function FontAWIcon(props) {
    return (
      <View>
        <FontAwesomeIcon 
        icon={props.name} 
        color={props.bottomBar == true ? (props.focused? Colors.tabIconSelected : Colors.tabIconDefault) : (props.color)}
        size={props.size == null ? 24 : props.size }
        style={props.style}
        />
      </View>
    );
  }
  


