import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
    Dimensions,
    Modal,
    ScrollView,
    TouchableWithoutFeedback
  } from 'react-native';
import { AsyncStorage } from 'react-native';
import FontAWIcon from './FontAWIcon';
import queryString from 'query-string';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as Actions from '../redux/actions/ReduxActionController';

class EditarNomeregua extends Component {
    
    constructor(props) {
        super(props);
        // console.log(props)
        regua = this.props.regua;
        
    }
    state = {
      text:""+this.props.regua.nome,
      onError:""
   };
   
    onChangeText(text){
      this.setState((previousState)=>{
        return {
          text:text
        }
      })
    }

    async removerRegua(){
      await this.atualizarReguaOnline(this.props.usuario.id);
      this.props.DesconfigurarRegua(Object.assign({},this.props.usuario));     
    }


    async atualizarReguaOnline(idUsuario){
      try{
        let value = await fetch('https://www.brunos4ntos.com/arduino/usuario/atualizarUsuario.php', {
            method: 'POST',   
            body: queryString.stringify({
              idUsuario:idUsuario,
              temRegua:0
            }),
            headers:{
              'Content-Type': 'application/x-www-form-urlencoded'
            }
          })
          .then((response) => {
            return response.text();
          })
          .then((json) => {
            return json;
          })
          .catch((e) => {
            // console.log(e)
            return {
              status:"error",
              message:"Erro ao Atualizar status da tomada durante a requisição"
            }
          });
          return JSON.parse(value)
        }catch(e){
          return {
            status:"error",
            message:"Não foi possivel enviar sua requisição"
          }
      }
    }

    onTrySubmit(){
      if(this.state.text.length < 5){
        this.setState({onError:"Insira no minimo 5 caracteres"})
        return;
      }
      this.props.UpdateNomeRegua(Object.assign({},this.props.regua),this.state.text);     
      this.props.setModalInvisible(false)
    }

    render() {
        
        this.regua = this.props.regua;
        return (
            <View>
              <View style={{padding:20}}>
                <Text style={{ marginLeft:8,marginBottom:25,fontSize:14,fontWeight:'bold' }} >Apelido da Régua</Text>
                <TextInput
                  style={{ height: 40, borderColor: 'gray', borderWidth:2,
                  borderRadius:15,padding:10 }}
                  onChangeText={(valueText) =>{
                    this.onChangeText(valueText)
                  }}
                  value={this.state.text}
                  maxLength={20}
                />
                <Text style={{ marginLeft:8,marginBottom:25,fontSize:12,color:'#E77010',fontWeight:'bold' }} >{this.state.onError}</Text>
              </View>
              <View style={{flexDirection: 'row', justifyContent: 'space-between',padding:20}}>
                <View style={{
                        height:55,
                        width:95,
                        backgroundColor:'#E77010',
                        borderRadius:30,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <TouchableOpacity onPressIn={()=>{
                        this.removerRegua()
                      }}> 
                              <Text style={{ fontSize:16,color:'#ffffff',fontWeight:'bold' }}>Remover</Text>
                      </TouchableOpacity>
                  </View> 
                  <View style={{
                        height:55,
                        width:95,
                        backgroundColor:'#C5E0B4',
                        borderRadius:30,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <TouchableOpacity onPressIn={()=>{
                        this.onTrySubmit(false)
                      }}> 
                              <Text style={{ fontSize:16,color:'#ffffff',fontWeight:'bold' }}>Atualizar</Text>
                      </TouchableOpacity>
                  </View> 
              </View>
            </View>
        );
    }
}


const mapStateToProps = state => ({ regua:state.regua,usuario: state.usuario });

const mapDispatchToProps = dispatch =>  bindActionCreators(Actions, dispatch);


export default connect(mapStateToProps,mapDispatchToProps)(EditarNomeregua)

const styles = StyleSheet.create({
    containerModal:{
        flex: 1,
        backgroundColor:'#eff0f1c9',
        maxHeight:Dimensions.get('window').height,
        height:'auto'
    },
    
});
  