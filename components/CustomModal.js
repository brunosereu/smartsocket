import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Dimensions,
    Modal,
    ScrollView,
    TouchableWithoutFeedback
  } from 'react-native';
import { AsyncStorage } from 'react-native';
import FontAWIcon from './FontAWIcon';
import ToggleSwitch from 'toggle-switch-react-native'

export default class CustomModal extends Component {
    
    constructor(props) {
        super(props);
        this.modalHeader = React.createRef()
    }

    state = {
        modalVisible: false,
        title:'',
        Content:View
    };
    
    setModalVisible = (newStatusVisible) =>{
      this.setState({modalVisible:newStatusVisible});
    }

    openModal(modal) {
        this.setState({modalVisible: modal.modalVisible});
        this.setState({title: modal.title});
        this.setState({Content: modal.conteudo != null ? modal.conteudo : View});
    }



    render() {
        this.regua = this.state.regua;
        return (
            <View>
                <Modal animationType="fade"
                  transparent={true}
                  visible={this.state.modalVisible}
                  onRequestClose={() => {this.setModalVisible(false)}}
                >
                  
                    <View  style={styles.containerModal} >
                     
                      <View  style={styles.Modal}>
                        <View style={{flexDirection: 'row', paddingTop:15, padding:15}}>
                          <TouchableOpacity
                            onPress={() => {
                              this.setModalVisible(!this.state.modalVisible);
                            }}>
                            <FontAWIcon name={"arrow-left"} style={styles.ArrowLeft} size={20} />
                          </TouchableOpacity>

                          <Text style={styles.ModalTitle}>{this.state.title}</Text>
                        </View>
                        
                        <this.state.Content setModalInvisible={this.setModalVisible} props={ this.props }/>
                        
                      </View>
                    </View>
                </Modal>
               
              </View>
        );
    }
}

const styles = StyleSheet.create({
    containerModal:{
        flex: 1,
        backgroundColor:'#eff0f1c9',
        maxHeight:Dimensions.get('window').height,
        height:'auto'
    },
    Modal:{
        height:'auto',
        marginTop:80,
        margin:15,
        backgroundColor:'#fff',
        padding:10,
        borderRadius:15,
    },
    ModalTitle:{
      fontWeight:'bold',
      fontSize:20,
      paddingLeft:10
    },
    ArrowLeft:{
      marginTop:4
    }
});
  