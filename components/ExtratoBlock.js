import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
  } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../redux/actions/ReduxActionController';


class ExtratoBlock extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            isVisible: false 
        };
    }
    
    customround (number, precision) {
      var factor = Math.pow(10, precision);
      var tempNumber = number * factor;
      var roundedTempNumber = Math.round(tempNumber);
      return roundedTempNumber / factor;
    };

    render() {
        let e = this.props.ExtratoBlock;
        return (
          <View style={{alignItems:'center'}}>
            <View style={{
              width:320,
              height:80,
              marginTop:20,
              borderColor: '#707070',
              borderWidth:2,
              borderRadius:15,
            }}
            >
              <TouchableOpacity   onPress={() => {
                    this.setState({
                      isVisible: !this.state.isVisible
                    })
                }}> 
                    <View style={{
                      width:280,
                      marginLeft:20,
                      height:70,
                      flexDirection: 'row',
                    justifyContent: 'space-between'}}>
                      <View  style={{flexDirection: 'column',backgroundColor:'', justifyContent: 'space-between',alignItems:'center',paddingTop:13,paddingLeft:0,paddingBottom:20,paddingRight:10}}>
                        <Text style={{ fontSize:20,paddingLeft:0,marginTop:10,color: '#707070',fontWeight:'bold' }}> 
                        {e.DataHora.slice(0,5)}
                        </Text> 
                      </View>
                      <View  style={{flexDirection: 'column',backgroundColor:'', justifyContent: 'space-between',alignItems:'center',paddingTop:20,paddingLeft:0,paddingBottom:10,paddingRight:5}}>
                      <Text style={{ fontSize:18,fontWeight:'bold',color: '#707070' }}>{this.customround(Number(e.Tomada1Watts)+Number(e.Tomada2Watts),2)} W</Text>
                      <Text style={{ fontSize:12,fontWeight:'bold',color: '#707070' }}>Consumido</Text>
                      </View>
                      <View  style={{flexDirection: 'column', justifyContent: 'space-between',alignItems:'center',paddingTop:20,paddingLeft:10,paddingBottom:20,paddingRight:10}}>
                    <Text style={{ fontSize:18,fontWeight:'bold',color: '#707070' }}>R$ {
                      this.customround(this.customround(0.516*(Number(e.Tomada1Watts)/1000),2)+this.customround(0.516*(Number(e.Tomada2Watts)/1000),2),2).toFixed(2)
                    }</Text>
                      <Text style={{ fontSize:12,fontWeight:'bold',color: '#707070' }}>Valor</Text>
                      </View>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={this.state.isVisible == true ? {
              width:280,
              height:'auto',
              minHeight:60,
              marginTop:-5,
              borderColor: '#707070',
              borderTopColor: 'transparent',
              borderWidth:2,
              borderRadius:10,
            }:{display:'none'}}
            >
                <View  style={{flexDirection: 'row',backgroundColor:'', justifyContent: 'center',alignItems:'center',paddingTop:13,paddingLeft:0,paddingBottom:10,paddingRight:10}}>
                      <View  style={{flexDirection: 'column',backgroundColor:'', justifyContent: 'space-between',alignItems:'center',paddingTop:0,paddingLeft:0,paddingBottom:10, paddingRight:5}}>
                          <Text style={{ fontSize:14,paddingRight:10,marginTop:10,color: '#E77010',fontWeight:'bold' }}> 
                            {e.tomada1Nome}
                            </Text> 
                        </View>
                        <View  style={{flexDirection: 'column',backgroundColor:'', justifyContent: 'space-between',alignItems:'center',paddingTop:20,paddingLeft:10,paddingBottom:10,paddingRight:5}}>
                            <Text style={{ fontSize:14,fontWeight:'bold',color: '#E77010' }}>{e.Tomada1Watts} W</Text>
                            <Text style={{ fontSize:12,fontWeight:'bold',color: '#E77010' }}>Consumido</Text>
                        </View>
                        <View  style={{flexDirection: 'column',backgroundColor:'', justifyContent: 'space-between',alignItems:'center',paddingTop:20,paddingLeft:20,paddingBottom:10,paddingRight:0}}>
                            <Text style={{ fontSize:14,fontWeight:'bold',color: '#E77010' }}>{e.Tomada1KWh}</Text>
                            <Text style={{ fontSize:12,fontWeight:'bold',color: '#E77010' }}>Valor</Text>
                        </View>
                  </View>
                  <View  style={{flexDirection: 'row',backgroundColor:'', justifyContent: 'center',alignItems:'center',paddingTop:0,paddingLeft:0,paddingBottom:20,paddingRight:10}}>
                      <View  style={{flexDirection: 'column',backgroundColor:'', justifyContent: 'space-between',alignItems:'center',paddingTop:0,paddingLeft:0,paddingBottom:10, paddingRight:5}}>
                          <Text style={{ fontSize:14,paddingRight:10,marginTop:10,color: '#9FE3B4',fontWeight:'bold' }}> 
                           {e.tomada2Nome}
                            </Text> 
                        </View>
                        <View  style={{flexDirection: 'column',backgroundColor:'', justifyContent: 'space-between',alignItems:'center',paddingTop:20,paddingLeft:10,paddingBottom:10,paddingRight:5}}>
                            <Text style={{ fontSize:14,fontWeight:'bold',color: '#9FE3B4' }}>{e.Tomada2Watts} W</Text>
                            <Text style={{ fontSize:12,fontWeight:'bold',color: '#9FE3B4' }}>Consumido</Text>
                        </View>
                        <View  style={{flexDirection: 'column',backgroundColor:'', justifyContent: 'space-between',alignItems:'center',paddingTop:20,paddingLeft:20,paddingBottom:10,paddingRight:0}}>
                            <Text style={{ fontSize:14,fontWeight:'bold',color: '#9FE3B4' }}>{e.Tomada2KWh}</Text>
                            <Text style={{ fontSize:12,fontWeight:'bold',color: '#9FE3B4' }}>Valor</Text>
                        </View>
                  </View>
            </View>
          </View>
        );
    }
}

const mapStateToProps = state => ({ regua:state.regua });

const mapDispatchToProps = dispatch =>  bindActionCreators(Actions, dispatch);

export default connect(mapStateToProps,mapDispatchToProps)(ExtratoBlock);

const styles = StyleSheet.create({
    headerContainer: {
      minHeight:160,
      padding:15,
      backgroundColor:'#9FE3B4'
    },
});
  