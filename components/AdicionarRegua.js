import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
    Dimensions,
    Modal,
    ScrollView,
    TouchableWithoutFeedback
  } from 'react-native';
import { AsyncStorage } from 'react-native';
import FontAWIcon from './FontAWIcon';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as Actions from '../redux/actions/ReduxActionController';

class EditarNomeregua extends Component {
    
    constructor(props) {
        super(props);
        // console.log(props)
        regua = this.props.regua;
        
    }
    state = {
      text:"",
      onError:""
   };
   
    onChangeText(text){
      this.setState((previousState)=>{
        return {
          text:text
        }
      })
    }

    onTrySubmit(){
      this.setState({onError:""});
      if(this.state.text.length == 0){
        this.setState({onError:"Entre com um ID"});
      }
     
      setTimeout(function(){ this.setState({onError:"ID não encontrado"}); }.bind(this), 2000);

      return false;
    }
    render() {
        
        this.regua = this.props.regua;
        return (
            <View>
              <View style={{padding:20}}>
                <Text style={{ marginLeft:8,marginBottom:25,fontSize:14,fontWeight:'bold' }} >ID da Regua</Text>
                <TextInput
                  style={{ height: 40, borderColor: 'gray', borderWidth:2,
                  borderRadius:15,padding:10 }}
                  onChangeText={(valueText) =>{
                    this.onChangeText(valueText)
                  }}
                  value={this.state.text}
                  maxLength={20}
                />
                <Text style={{ marginLeft:8,marginBottom:25,fontSize:12,color:'#E77010',fontWeight:'bold' }} >{this.state.onError}</Text>
              </View>
              <View style={{flexDirection: 'row-reverse', justifyContent: 'space-between',padding:20}}>
                  <View style={{
                        height:45,
                        width:105,
                        backgroundColor:'#C5E0B4',
                        borderRadius:30,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <TouchableOpacity onPressIn={()=>{
                        this.onTrySubmit()
                      }}> 
                              <Text style={{ fontSize:15,color:'#ffffff',fontWeight:'bold' }}>Adicionar</Text>
                      </TouchableOpacity>
                  </View> 
              </View>
            </View>
        );
    }
}


const mapStateToProps = state => ({ regua:state.regua });

const mapDispatchToProps = dispatch =>  bindActionCreators(Actions, dispatch);


export default connect(mapStateToProps,mapDispatchToProps)(EditarNomeregua)

const styles = StyleSheet.create({
    containerModal:{
        flex: 1,
        backgroundColor:'#eff0f1c9',
        maxHeight:Dimensions.get('window').height,
        height:'auto'
    },
    
});
  