import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Dimensions,
    Modal,
  } from 'react-native';
import { AsyncStorage } from 'react-native';
import FontAWIcon from '../components/FontAWIcon';
import ModalDropdown from 'react-native-modal-dropdown';
import { showMessage, hideMessage } from "react-native-flash-message";


import CustomModal from './CustomModal';
import EditarNomeregua from './EditarNomeregua';
import SelecionarRegua from './SelecionarRegua';
import About from './modalScreens/modalAbout';
import ConfigAccount from './modalScreens/modalConfigAccount';


import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as Actions from '../redux/actions/ReduxActionController';

class HeaderComponnent extends Component {
    
    constructor(props) {
        super(props);
        this.modalHeader = React.createRef();
        
       
    }

    async selectedItem(e,value){
      if(e == 0){
        await this.updateValue()
      }else if(e == 1){
        this.modalHeader.current.openModal({
          title: 'Configurações da Conta',
          modalVisible: !this.modalHeader.current.state.modalVisible,
          conteudo:ConfigAccount
        });
      }else{
        this.modalHeader.current.openModal({
          title: 'Sobre',
          modalVisible: !this.modalHeader.current.state.modalVisible,
          conteudo:About
        });
      }

      return false
    }

    async updateValue(){
        showMessage({
          message: "Atualizando Valores de Consumo...",
          duration:2500,
          type: "info",
          backgroundColor: "#17a2b8", // background color
          color: "#ffff", // text color
        });
        
        let lastStatusTomada = await this.updateLastStatusTomada();
        let lastGraph = await this.updateLastGraph();
        let lastMounth = await this.updateMounthConsumo();
        if(lastStatusTomada.status=="ok" && lastGraph.status == "ok" && lastMounth.status == "ok"){
          this.props.UpdateStatusOnlineTomada(Object.assign({},this.props.regua),lastStatusTomada.json);   
          this.props.UpdateValuesGraficoSemanal(Object.assign({},this.props.regua),lastGraph);    
          this.props.UpdateValues(Object.assign({},this.props.regua),lastMounth);    
        }else{
          showMessage({
            message: "Ocorreu um Erro",
            description: "Não foi possivel atualizar seus dados",
            duration:2500,
            type: "default",
            backgroundColor: "#E77010", // background color
            color: "#ffff", // text color
          });
        }
        
    }

    async updateLastGraph(){
      try{
        let value = await fetch('http://brunos4ntos.com/arduino/graficoDaSemanaDeConsumo.php', {
          method: 'GET',   
          headers:{
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        })
        .then((response) => {
          return response.text();
        })
        .then((json) => {
          // console.log(JSON.parse(json));
          return {
            status:'ok',
            json:JSON.parse(json)
          };
        })
        .catch((error) => {
          return {
            status:"error",
            message:"Erro ao retornar dados"
          }
        });
        return value
      }catch(e){
        return {
          status:"error",
          message:"Não foi possivel enviar sua requisição"
        }
      }
    }


    async updateLastStatusTomada(){
      try{
        let value = await fetch('http://brunos4ntos.com/arduino/statusTomada.php', {
          method: 'GET',   
          headers:{
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        })
        .then((response) => {
          return response.text();
        })
        .then((json) => {
          // console.log(JSON.parse(json));
          return {
            status:'ok',
            json:JSON.parse(json)
          };
        })
        .catch((error) => {
          return {
            status:"error",
            message:"Erro ao retornar dados"
          }
        });
        return value
      }catch(e){
        return {
          status:"error",
          message:"Não foi possivel enviar sua requisição"
        }
      }
    }

    async updateMounthConsumo(){
      try{
        let value = await fetch('http://brunos4ntos.com/arduino/DadosDoMesAtual.php', {
          method: 'GET',   
          headers:{
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        })
        .then((response) => {
          return response.text();
        })
        .then((json) => {
          // console.log(JSON.parse(json));
          return {
            status:'ok',
            json:JSON.parse(json)
          };
        })
        .catch((error) => {
          return {
            status:"error",
            message:"Erro ao retornar dados"
          }
        });
        return value
      }catch(e){
        return {
          status:"error",
          message:"Não foi possivel enviar sua requisição"
        }
      }
    }

    render() {
        this.regua = this.props.regua;
        return (
            <View style={styles.headerContainer}>
                <View style={{  flex:1, }}>
                  <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                      <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                      <TouchableOpacity   onPress={() => {
                          this.modalHeader.current.openModal({
                            title: 'Editar sua Régua',
                            modalVisible: !this.modalHeader.current.state.modalVisible,
                            conteudo: EditarNomeregua
                          });
                      }}> 
                        <Text style={styles.HEtextTomada}>{this.regua.nome}</Text>
                      </TouchableOpacity>
                      <TouchableOpacity   onPress={() => {
                          this.modalHeader.current.openModal({
                            title: 'Selecione sua Régua',
                            modalVisible: !this.modalHeader.current.state.modalVisible,
                            conteudo: SelecionarRegua
                          });
                      }}> 
                        <View styel={{margin:2}}>
                         <FontAWIcon name={"chevron-down"} style={styles.HEdropDownTomadas} size={18} />
                        </View>
                      </TouchableOpacity>
                      </View>
                      <View>
                         <ModalDropdown ref="modalDropdown" 
                          style={styles.HEmodalDropDown} 
                          dropdownStyle={styles.dropDownStyle} 
                          defaultIndex={-1}
                          options={[
                            "Atualizar Dados"
                            ,
                            "Configurações"
                            ,"Sobre"
                          ]} 
                          onSelect={ (e,value) => { 
                              this.selectedItem(e,value);
                              return  false
                            
                            }
                          }
                            
                            defaultValue={
                              <View style={{ width:18,height:18}}>
                              
                              <FontAWIcon name={"ellipsis-v"} style={styles.HEopcoes} size={18} />
                          
                              </View>
                              }
                          > 
                          {/* <TouchableOpacity onPress={async () =>{ await this.updateValue()}}  style={styles.HEmodalDropDown} > 
                              <View style={{ width:18,height:18}}>
                              
                              <FontAWIcon name={"sync-alt"} style={styles.HEopcoes} size={18} />
                          
                              </View>
                          </TouchableOpacity> */}
                         
                          </ModalDropdown>
                      </View>
                  </View>
                  <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop:18,padding:15}}>
                      <View style={{flexDirection: 'column', justifyContent: 'space-between',alignItems:'center'}}>
                      <Text style={styles.HEtextValorConsumo}>{this.regua.consumoEsteMes} W</Text>
                      <Text style={styles.HEtextConsumo}>Consumo</Text>
                      </View>
                      <View style={{flexDirection: 'column', justifyContent: 'space-between',alignItems:'center'}}>
                      <Text style={styles.HEtextValorConsumo}>{this.regua.custoPrevisto}</Text>
                      <Text style={styles.HEtextConsumo}>Custo previsto mês</Text>
                      </View>
                  </View>
                  <View style = {{ alignItems:'flex-start', }}>
                    <Text style={styles.HElastUpdate}>Atualizado em { this.regua.lastUpdate }</Text>
                  </View>
                </View>
                
                <CustomModal ref={this.modalHeader}></CustomModal>
            </View>
        );
    }
}

const mapStateToProps = state => ({ regua:state.regua,usuario:state.usuario });

const mapDispatchToProps = dispatch =>  bindActionCreators(Actions, dispatch);


export default connect(mapStateToProps,mapDispatchToProps)(HeaderComponnent)




const styles = StyleSheet.create({
    headerContainer: {
      minHeight:160,
      padding:15,
      backgroundColor:'#C5E0B4'
    },
    HEtextTomada:{
      fontWeight:'bold',
      color:'#ffff',
      fontSize:20,
      paddingTop:10
      // paddingLeft: 50,
    },
    HEtextValorConsumo:{
      color:'#ffff',
      fontSize:18,
      fontWeight:'bold',
    },
    HEtextConsumo:{
      color:'#ffff',
      fontSize:14,
      fontWeight:'bold',
    },
    HEtextOpcoesInterno:{
      color:'#000000',
      fontSize:8,
    },
    HEdropDownTomadas:{
      color:'#ffff',
      marginLeft:15,
      marginTop:17
    },
    HEopcoes:{
      color:'#ffff',
      marginRight:10
    },
    HEopcoesInterno:{
      color:'#ffff',
      marginRight:10
    },
    HEopcoesBlock:{
      width:8,
      height:8
    },
    HEmodalDropDown:{
      width:25,
      height:18,
      marginTop:15
    },
    dropDownStyle:{
      marginTop:-30,
      width:120,
      height:125,
      borderRadius:20,
      borderColor:'white',
      borderWidth:8
    },
    HElastUpdate:{
      fontSize:10,
      marginBottom:2,
      color:'#ffff',
    }
});
  