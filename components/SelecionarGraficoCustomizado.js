import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
    Dimensions,
    Modal,
    ScrollView,
    TouchableWithoutFeedback
  } from 'react-native';
import { AsyncStorage } from 'react-native';
import FontAWIcon from './FontAWIcon';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import queryString from 'query-string';
import Moment from 'moment';

import * as Actions from '../redux/actions/ReduxActionController';

class SelecionarGraficoCustomizado extends Component {
    
    constructor(props) {
        super(props);
        this.OpenCustomGraph = this.props.props.props.OpenCustomGraph
        // this.OpenCustomGraph('teste');
        // tomada = this.props.props.props.tomada;
        
    }
    state = {
        Data1:"Escolha sua data",
        Data2:"Escolha sua data",
        onError1:"",
        onError2:"",
        isDatePicker1Visible:false,
        isDatePicker2Visible:false,
        timeToStore1:null,
        timeToStore2:null,
    };
    customround (number, precision) {
        var factor = Math.pow(10, precision);
        var tempNumber = number * factor;
        var roundedTempNumber = Math.round(tempNumber);
        return roundedTempNumber / factor;
    };
    openTimePicker(v){
      v == 1 ? 
      this.setState({isDatePicker1Visible:!this.state.isDatePicker1Visible})
      :
      this.setState({isDatePicker2Visible:!this.state.isDatePicker2Visible})
    }


    async onTrySubmit(){
      if(this.state.timeToStore1 == null){
        this.setState({onError1:"Selecione a data inicial em que desja visualizar seu consumo"})
        return;
      }
      this.setState({onError1:""});
      if(this.state.timeToStore2 == null){
        this.setState({onError2:"Selecione a data final em que deseja visualizar seu consumo"})
        return;
      }
      this.setState({onError2:""});
      if((this.state.timeToStore2 - this.state.timeToStore1) < 0){
        this.setState({onError2:"Selecione uma data final em que a mesma, seja maior ou igual a data inicial"})
        return;
      }
      this.setState({onError2:""});
      if((this.state.timeToStore2 - Moment()) > 0){
        this.setState({onError2:"Selecione uma data final igual ou menor a data de hoje"})
        return;
      }
      this.setState({onError2:""});
      let diffdates = this.customround(((((this.state.timeToStore2 - this.state.timeToStore1)/1000)/60)/60)/24,0)
      if(diffdates > 10){
        this.setState({onError2:"A diferença entre as datas não deve ser maior que 14 dias"})
        return;
      }
      this.setState({onError2:""});
      
      var ret = await this.getValuesBetweenDates({
        dataInicial:this.state.Data1,
        dataFinal:this.state.Data2
      });

      this.props.setModalInvisible(false)
      this.OpenCustomGraph(ret.json);
    }



    async getValuesBetweenDates(ctn){
      try{
        let value = await fetch(`http://brunos4ntos.com/arduino/graficoCustomizadoDeConsumo.php?dataInicial=${ctn.dataInicial}&dataFinal=${ctn.dataFinal}`, {
          method: 'GET',   
          headers:{
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        })
        .then((response) => {
          return response.text();
        })
        .then((json) => {
          // console.log(JSON.parse(json));
          return {
            status:'ok',
            json:JSON.parse(json)
          };
        })
        .catch((error) => {
          return {
            status:"error",
            message:"Erro ao retornar dados"
          }
        });
        return value
      }catch(e){
        return {
          status:"error",
          message:"Não foi possivel enviar sua requisição"
        }
      }
    }


    render() {
        
        // this.tomada = this.props.props.props.tomada;
        return (
            <View>
              <View style={{padding:20,paddingBottom:10}}>
                <Text style={{ marginLeft:8,marginBottom:25,fontSize:14,fontWeight:'bold' }} >Selecione a data mais antiga:</Text>
                <View
                  style={{ height: 40, borderColor: 'gray', borderWidth:2,
                  borderRadius:15,padding:10 }}
                >
                      <TouchableOpacity style={{}} onPressIn={()=>{
                        this.openTimePicker(1)
                      }}> 
                          <Text style={{ fontSize:11,}}>{this.state.Data1}</Text>
                      </TouchableOpacity>
                </View> 
                <DateTimePickerModal
                    isVisible={this.state.isDatePicker1Visible}
                    mode="date"
                    date={this.state.timeToStore1 == null  ? new Date() : Moment(this.state.timeToStore1).toDate()}
                    onConfirm={(time)=>{
                      this.openTimePicker(1)
                      this.setState({timeToStore1:time})
                      this.setState({onError1:""})
                    }}
                    onHide={()=>{
                      if(this.state.timeToStore1 != null)
                      this.setState({Data1:""+Moment(this.state.timeToStore1).format('DD/MM/yyyy')});
                    }}
                    onCancel={()=>{
                      this.openTimePicker(1)
                    }}
                />
                <Text style={{ marginLeft:8,marginBottom:25,fontSize:12,color:'#E77010',fontWeight:'bold' }} >{this.state.onError1}</Text>
              </View>
              <View style={{padding:20,paddingTop:0,paddingBottom:0}}>
                <Text style={{ marginLeft:8,marginBottom:25,fontSize:14,fontWeight:'bold' }} >Selecione a data mais recente:</Text>
                <View
                  style={{ height: 40, borderColor: 'gray', borderWidth:2,
                  borderRadius:15,padding:10 }}
                >
                      <TouchableOpacity style={{}} onPressIn={()=>{
                        this.openTimePicker(2)
                      }}> 
                          <Text style={{ fontSize:11,}}>{this.state.Data2}</Text>
                      </TouchableOpacity>
                </View> 
                <DateTimePickerModal
                    isVisible={this.state.isDatePicker2Visible}
                    mode="date"
                    date={this.state.timeToStore2 == null  ? new Date() : Moment(this.state.timeToStore2).toDate()}
                    onConfirm={(time)=>{
                      this.openTimePicker(2)
                      this.setState({timeToStore2:time})
                      this.setState({onError1:""})
                    }}
                    onHide={()=>{
                      if(this.state.timeToStore2 != null)
                      this.setState({Data2:""+Moment(this.state.timeToStore2).format('DD/MM/yyyy')});
                    }}
                    onCancel={()=>{
                      this.openTimePicker(2)
                    }}
                />
                <Text style={{ marginLeft:8,marginBottom:25,fontSize:12,color:'#E77010',fontWeight:'bold' }} >{this.state.onError2}</Text>
              </View>
              <View style={{flexDirection: 'row', justifyContent: 'center',padding:20}}>
                  <View style={{
                        height:55,
                        width:95,
                        backgroundColor:'#C5E0B4',
                        borderRadius:30,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <TouchableOpacity onPressIn={()=>{
                        this.onTrySubmit(false)
                      }}> 
                              <Text style={{ fontSize:16,color:'#ffffff',fontWeight:'bold' }}>Visualizar</Text>
                      </TouchableOpacity>
                  </View> 
              </View>
            </View>
        );
    }
}


const mapStateToProps = state => ({ regua:state.regua });

const mapDispatchToProps = dispatch =>  bindActionCreators(Actions, dispatch);


export default connect(mapStateToProps,mapDispatchToProps)(SelecionarGraficoCustomizado)

const styles = StyleSheet.create({
    containerModal:{
        flex: 1,
        backgroundColor:'#eff0f1c9',
        maxHeight:Dimensions.get('window').height,
        height:'auto'
    },
    
});
  