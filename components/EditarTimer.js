import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    CheckBox,
    TouchableOpacity,
    View,
    Dimensions,
    Modal,
    ScrollView,
    TouchableWithoutFeedback
  } from 'react-native';
import { AsyncStorage } from 'react-native';
import FontAWIcon from './FontAWIcon';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import queryString from 'query-string';
import Moment from 'moment';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as Actions from '../redux/actions/ReduxActionController';

class EditarTimer extends Component {
    
    constructor(props) {
        super(props);
        this.timer =  this.props.props.props.Timer;
        // console.log(this.timer)
    }
    state = {
        id:this.props.props.props.Timer.idTimer,
        status:this.props.props.props.Timer.status,
        text:""+this.props.props.props.Timer.hora,
        onError1:"",
        onError2:"",
        isDatePickerVisible:false,
        timeToStore: Moment(this.props.props.props.Timer.hora,"hh:mm").toDate(),
        ActionValue:this.props.props.props.Timer.acao,
        CheckBox1:!!this.props.props.props.Timer.tomada1,
        CheckBox2:!!this.props.props.props.Timer.tomada2
    };
    
    openTimePicker(value){
      this.setState({isDatePickerVisible:value})
    }

    async tryEditarTimer(){
      if(this.state.timeToStore == null){
        this.setState({onError1:"Selecione o Horário em que desja agendar a rotina"})
        return;
      }
      if(this.state.CheckBox1 == false && this.state.CheckBox2 == false){
        this.setState({onError2:"Selecione pelo menos uma tomada em que desja agendar a rotina"})
        return;
      }
      // await _addTimer()
      var timerStructure = {
        idTimer:this.state.id,
        acao:this.state.ActionValue,
        status:this.state.status,
        tomada1: this.state.CheckBox1 != false ? 1 : 0,
        tomada2: this.state.CheckBox2 != false ? 1 : 0,
        hora:''+Moment(this.state.timeToStore).format('HH:mm')
      }
      console.log(timerStructure)
      await this._editTimer(timerStructure);
      this.props.UpdateTimer(Object.assign({},this.props.regua),timerStructure);     
      this.props.setModalInvisible(false)
    }


    async excluirTimer(){
      if(this.state.timeToStore == null){
        this.setState({onError1:"Selecione o Horário em que desja agendar a rotina"})
        return;
      }
      if(this.state.CheckBox1 == false && this.state.CheckBox2 == false){
        this.setState({onError2:"Selecione pelo menos uma tomada em que desja agendar a rotina"})
        return;
      }
      // await _addTimer()
      var timerStructure = {
        idTimer:this.state.id,
        acao:this.state.ActionValue,
        status:this.state.status,
        tomada1: this.state.CheckBox1 != false ? 1 : 0,
        tomada2: this.state.CheckBox2 != false ? 1 : 0,
        hora:''+Moment(this.state.timeToStore).format('HH:mm')
      }
     
      await this._deleteTimer(timerStructure);
      this.props.DeleteTimer(Object.assign({},this.props.regua),timerStructure);     
      this.props.setModalInvisible(false)
    }


    async _editTimer(ctn){
      try{
        let value = await fetch('https://www.brunos4ntos.com/arduino/timer/atualizaTimer.php', {
            method: 'POST',   
            body: queryString.stringify(ctn),
            headers:{
              'Content-Type': 'application/x-www-form-urlencoded'
            }
          })
          .then((response) => {
            return response.text();
          })
          .then((json) => {
            // console.log(json);
            return json;
          })
          .catch((error) => {
            return {
              status:"error",
              message:"Erro ao Atualizar status da tomada durante a requisição"
            }
          });
          return JSON.parse(value)
        }catch(e){
          return {
            status:"error",
            message:"Não foi possivel enviar sua requisição"
          }
        }
    }

    async _deleteTimer(ctn){
      try{
        let value = await fetch('https://www.brunos4ntos.com/arduino/timer/excluirTimer.php', {
            method: 'POST',   
            body: queryString.stringify(ctn),
            headers:{
              'Content-Type': 'application/x-www-form-urlencoded'
            }
          })
          .then((response) => {
            return response.text();
          })
          .then((json) => {
            console.log(json);
            return json;
          })
          .catch((error) => {
            return {
              status:"error",
              message:"Erro ao Atualizar status da tomada durante a requisição"
            }
          });
          return JSON.parse(value)
        }catch(e){
          return {
            status:"error",
            message:"Não foi possivel enviar sua requisição"
          }
        }
    }


    render() {
      
        var radio_props = [
          {label: 'Ligar               ', value: 1 },
          {label: 'Desligar', value: 0 }
        ];
        this.tomada = [];
        // console.log(this.state)
        return (
            <View>
              <View style={{padding:20,paddingBottom:0}}>
                <Text style={{ marginLeft:8,marginBottom:25,fontSize:14,fontWeight:'bold' }} >Horário Desejado</Text>
                <View
                  style={{ height: 40, borderColor: 'gray', borderWidth:2,
                  borderRadius:15,padding:10 }}
                >
                      <TouchableOpacity style={{}} onPressIn={()=>{
                        this.openTimePicker(true)
                      }}> 
                          <Text style={{ fontSize:11,}}>{this.state.text}</Text>
                      </TouchableOpacity>
                </View> 
                <DateTimePickerModal
                    isVisible={this.state.isDatePickerVisible}
                    mode="time"
                    date={this.state.timeToStore == null ? new Date() : Moment(this.state.timeToStore).toDate()}
                    onConfirm={(time)=>{
                      this.openTimePicker(false)
                      this.setState({timeToStore:time})
                      this.setState({onError1:""})
                    }}
                    onHide={()=>{
                      this.openTimePicker(false)
                      if(this.state.timeToStore != null)
                      this.setState({text:""+Moment(this.state.timeToStore).format('HH:mm')});
                    }}
                    onCancel={()=>{
                      this.openTimePicker(false)
                    }}
                />
                <Text style={{ marginLeft:8,fontSize:12,color:'#E77010',fontWeight:'bold' }} >{this.state.onError1}</Text>
              </View>
              <View style={{padding:20,paddingBottom:5}}>
                <Text style={{ marginLeft:8,marginBottom:25,fontSize:14,fontWeight:'bold' }} >Ação</Text>
                <RadioForm
                  radio_props={radio_props}
                  initial={this.state.ActionValue == 1 ? 0 : 1}
                  buttonColor={'#707070'}
                  selectedButtonColor={'#707070'}
                  style={{padding:5}}
                  formHorizontal={true}
                  onPress={(value) => {
                    console.log(value)
                    this.setState({ActionValue:value})
                  }}
                />
              </View>
              <View style={{padding:20}}>
                <Text style={{ marginLeft:8,marginBottom:25,fontSize:14,fontWeight:'bold' }} >Tomadas</Text>
                <View style={{ flexDirection: "row",justifyContent: 'space-between'}}>
                  <View style={{paddingLeft:40,paddingRight:20}}>
                    <TouchableOpacity style={{alignItems:'center'}} onPressIn={()=>{
                        this.setState({CheckBox1:!this.state.CheckBox1})
                      }}> 
                          <View style={{
                            height:35,
                            width:35,
                            backgroundColor:'#fffff',
                            borderColor:"#00000",
                            borderRadius:0,
                            borderWidth:1,
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}>
                                <View style={{
                                height:30,
                                width:30,
                                backgroundColor: this.state.CheckBox1 === true ? '#ABE6BE' : 'white',
                                alignItems: 'center',
                                justifyContent: 'center',
                              }}></View>
                          </View>
                          <Text style={{alignContent:'center'}}>1</Text>
                    </TouchableOpacity>
                    
                  </View>
                  <View style={{paddingLeft:20,paddingRight:40}}>
                  <TouchableOpacity style={{alignItems:'center'}} onPressIn={()=>{
                        this.setState({CheckBox2:!this.state.CheckBox2})
                      }}> 
                          <View style={{
                            height:35,
                            width:35,
                            backgroundColor:'#fffff',
                            borderColor:"#00000",
                            borderRadius:0,
                            borderWidth:1,
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}>
                                <View style={{
                                height:30,
                                width:30,
                                backgroundColor: this.state.CheckBox2 === true ? '#ABE6BE' : 'white',
                                alignItems: 'center',
                                justifyContent: 'center',
                              }}></View>
                          </View>
                          <Text style={{alignContent:'center'}}>2</Text>
                    </TouchableOpacity>
                  </View>
                </View>
                <Text style={{ marginLeft:8,fontSize:12,color:'#E77010',fontWeight:'bold' }} >{this.state.onError2}</Text>
              </View>

              <View style={{flexDirection: 'row-reverse', justifyContent: 'space-between',padding:20}}>
                  
                  <View style={{
                        height:40,
                        width:120,
                        backgroundColor:'#C5E0B4',
                        borderRadius:30,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <TouchableOpacity onPressIn={()=>{
                        this.tryEditarTimer(false)
                      }}> 
                              <Text style={{ fontSize:16,color:'#ffffff',fontWeight:'bold' }}>Editar</Text>
                      </TouchableOpacity>
                  </View> 
                  <View style={{
                        height:40,
                        width:90,
                        backgroundColor:'#E77010',
                        borderRadius:30,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <TouchableOpacity onPressIn={()=>{
                        this.excluirTimer(false)
                      }}> 
                              <Text style={{ fontSize:16,color:'#ffffff',fontWeight:'bold' }}>Excluir</Text>
                      </TouchableOpacity>
                  </View> 
              </View>
            </View>
        );
    }
}


const mapStateToProps = state => ({ regua:state.regua });

const mapDispatchToProps = dispatch =>  bindActionCreators(Actions, dispatch);


export default connect(mapStateToProps,mapDispatchToProps)(EditarTimer)

const styles = StyleSheet.create({
    containerModal:{
        flex: 1,
        backgroundColor:'#eff0f1c9',
        maxHeight:Dimensions.get('window').height,
        height:'auto'
    },
    
});
  