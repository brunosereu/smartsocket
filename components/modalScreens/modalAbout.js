import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
    Dimensions,
    Modal,
    ScrollView,
    TouchableWithoutFeedback
  } from 'react-native';
import { AsyncStorage } from 'react-native';
import FontAWIcon from '../FontAWIcon';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as Actions from '../../redux/actions/ReduxActionController';

class About extends Component {
    
    constructor(props) {
        super(props);
        // console.log(props)
        regua = this.props.regua;
        
    }
    state = {
      text:""+this.props.regua.nome,
      onError:""
   };
   
    onChangeText(text){
      this.setState((previousState)=>{
        return {
          text:text
        }
      })
    }

    onTrySubmit(){
      if(this.state.text.length < 5){
        this.setState({onError:"Insira no minimo 5 caracteres"})
        return;
      }
      this.props.UpdateNomeRegua(Object.assign({},this.props.regua),this.state.text);     
      this.props.setModalInvisible(false)
    }
    render() {
        
        this.regua = this.props.regua;
        return (
            <View>
              <View style={{padding:20}}>
                <Text style={{ marginLeft:8,marginBottom:25,fontSize:14}} >Este aplicativo foi feito e idealizado por:</Text>
                <Text style={{ marginLeft:8,marginBottom:0,fontSize:14}} >- Bruno Santos</Text>
                <Text style={{ marginLeft:8,marginBottom:0,fontSize:14}} >- Ezequiel Rocha</Text>
                <Text style={{ marginLeft:8,marginBottom:0,fontSize:14}} >- Edvan Martins</Text>
                <Text style={{ marginLeft:8,marginBottom:0,fontSize:14}} >- Romildo Coelho</Text>
                <Text style={{ marginLeft:8,marginBottom:25,fontSize:14}} >- Victorio Villanova</Text>
                <Text style={{ marginLeft:8,marginBottom:25,fontSize:14}} >Smart Socket ©</Text>
              </View>
            </View>
        );
    }
}


const mapStateToProps = state => ({ regua:state.regua });

const mapDispatchToProps = dispatch =>  bindActionCreators(Actions, dispatch);


export default connect(mapStateToProps,mapDispatchToProps)(About)

const styles = StyleSheet.create({
    containerModal:{
        flex: 1,
        backgroundColor:'#eff0f1c9',
        maxHeight:Dimensions.get('window').height,
        height:'auto'
    },
    
});
  