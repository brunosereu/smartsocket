import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
    Dimensions,
    Modal,
    ScrollView,
    TouchableWithoutFeedback
  } from 'react-native';
import { AsyncStorage } from 'react-native';
import FontAWIcon from '../FontAWIcon';
import { showMessage, hideMessage } from "react-native-flash-message";

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as Actions from '../../redux/actions/ReduxActionController';

class ConfigAccount extends Component {
    
    constructor(props) {
        super(props);
        // console.log(props)
        regua = this.props.regua;
        
    }
    state = {
      text:""+this.props.regua.nome,
      username:""+this.props.usuario.nome,
      email:""+this.props.usuario.email,
      onError:""
   };
   
    onChangeText(value){
      this.setState(value)
    }

    onTrySubmit(){
      if(this.state.email == null){
        showMessage({
          message: "Verifique",
          description: "Preencha o campo de email",
          duration:2500,
          type: "default",
          backgroundColor: "#E77010", // background color
          color: "#ffff", // text color
        });
        return false
      }
      let email = this.state.email
      var ee = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);
      if(!ee.test(email) ) { 
        // alert('Preencha o campo email com um e-mail valido'); 
        showMessage({
          message: "Verifique",
          description: "Preencha o campo email com um e-mail valido",
          duration:2500,
          type: "default",
          backgroundColor: "#E77010", // background color
          color: "#ffff", // text color
        });
        return false; 
      }
      if(this.state.username == null){
        showMessage({
          message: "Verifique",
          description: "Preencha o campo de nome de usuário",
          duration:2500,
          type: "default",
          backgroundColor: "#E77010", // background color
          color: "#ffff", // text color
        });
        return false
      }
      var en = new RegExp(/^[A-Za-z0-9]*$/);
      let username = this.state.username
      if(!en.test(username) ) { 
        // alert('Preencha o campo email com um e-mail valido'); 
        showMessage({
          message: "Verifique",
          description: "Preencha o campo de nome de usuário com apenas números ou letras",
          duration:2500,
          type: "default",
          backgroundColor: "#E77010", // background color
          color: "#ffff", // text color
        });
        return false; 
      }
      if(this.state.senha1 != null && this.state.senha1 != "" ){
        if(this.state.senha1.length < 4){
          showMessage({
            message: "Verifique",
            description: "Insira uma senha maior que 4 caracteres",
            duration:2500,
            type: "default",
            backgroundColor: "#E77010", // background color
            color: "#ffff", // text color
          });
          return false
        }
        if(this.state.senha2 == "" || this.state.senha2 == null){
          showMessage({
            message: "Verifique",
            description: "Insira sua confirmação de senha",
            duration:2500,
            type: "default",
            backgroundColor: "#E77010", // background color
            color: "#ffff", // text color
          });
          return false
        }
        if(this.state.senha2 != this.state.senha1){
          showMessage({
            message: "Verifique",
            description: "Sua senha e sua confirmação de senha não coincidem",
            duration:2500,
            type: "default",
            backgroundColor: "#E77010", // background color
            color: "#ffff", // text color
          });
          return false
        }
        password = this.state.senha1
      }else {
        password = null
      }
      this.props.Logout(Object.assign({},this.props.usuario),{email,username,password});     
    }

    logout(){
      this.props.Logout(Object.assign({},this.props.usuario),{});     
    }

    render() {
        
        this.regua = this.props.regua;
        return (
            <View>
               <View style={{padding:10,paddingBottom:3}}>
                  <Text style={{ marginLeft:8,marginBottom:5,fontSize:14,fontWeight:'bold' }} >Nome de Usuario</Text>
                  <TextInput
                    style={{ height: 40, borderColor: 'gray', borderWidth:2,
                    borderRadius:15,padding:10 }}
                    onChangeText={(valueText) =>{
                      this.onChangeText({username:valueText})
                    }}
                    value={this.state.username}
                    maxLength={20}
                    placeholder='Nome de Usuário (Apenas números ou letras)'
                  />
                  <Text style={{ marginLeft:8,marginBottom:5,fontSize:12,color:'#E77010',fontWeight:'bold' }} >{this.state.onError}</Text>
                </View>
                <View style={{padding:10,paddingTop:3,paddingBottom:3}}>
                  <Text style={{ marginLeft:8,marginBottom:5,fontSize:14,fontWeight:'bold' }} >Email</Text>
                  <TextInput
                    style={{ height: 40, borderColor: 'gray', borderWidth:2,
                    borderRadius:15,padding:10 }}
                    onChangeText={(valueText) =>{
                      this.onChangeText({email:valueText})
                    }}
                    value={this.state.email}
                    maxLength={255}
                  />
                  <Text style={{ marginLeft:8,marginBottom:5,fontSize:12,color:'#E77010',fontWeight:'bold' }} >{this.state.onError}</Text>
                </View>
                <View style={{padding:10,paddingBottom:3,paddingTop:3}}>
                <Text style={{ marginLeft:8,marginBottom:5,fontSize:14,fontWeight:'bold' }} >Caso deseje alterar a senha:</Text>
                </View>

                <View style={{padding:10,paddingTop:3,paddingBottom:3}}>
                  <Text style={{ marginLeft:8,marginBottom:5,fontSize:14,fontWeight:'bold' }} >Senha</Text>
                  <TextInput
                    style={{ height: 40, borderColor: 'gray', borderWidth:2,
                    borderRadius:15,padding:10 }}
                    onChangeText={(valueText) =>{
                      this.onChangeText({senha1:valueText})
                    }}
                    maxLength={20}
                    secureTextEntry={true}
                  />
                  <Text style={{ marginLeft:8,marginBottom:5,fontSize:12,color:'#E77010',fontWeight:'bold' }} >{this.state.onError}</Text>
                </View>
                <View style={{padding:10,paddingTop:3,paddingBottom:3}}>
                  <Text style={{ marginLeft:8,marginBottom:5,fontSize:14,fontWeight:'bold' }} >Confirme sua senha</Text>
                  <TextInput
                    style={{ height: 40, borderColor: 'gray', borderWidth:2,
                    borderRadius:15,padding:10 }}
                    onChangeText={(valueText) =>{
                      this.onChangeText({senha2:valueText})
                    }}
                    maxLength={20}
                    secureTextEntry={true}
                  />
                  <Text style={{ marginLeft:8,marginBottom:5,fontSize:12,color:'#E77010',fontWeight:'bold' }} >{this.state.onError}</Text>
                </View>
                <View style={{flexDirection: 'row', justifyContent: 'space-between',padding:10}}>
                    <View style={{
                          height:45,
                          width:85,
                          backgroundColor:'#E77010',
                          borderRadius:30,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <TouchableOpacity onPressIn={()=>{
                          this.logout()
                        }}> 
                                <Text style={{ fontSize:16,color:'#ffffff',fontWeight:'bold' }}>Sair</Text>
                        </TouchableOpacity>
                    </View> 
                    <View style={{
                          height:45,
                          width:85,
                          backgroundColor:'#C5E0B4',
                          borderRadius:30,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <TouchableOpacity onPressIn={()=>{
                          this.onTrySubmit(false)
                        }}> 
                                <Text style={{ fontSize:16,color:'#ffffff',fontWeight:'bold' }}>Atualizar</Text>
                        </TouchableOpacity>
                    </View> 
                </View>
            </View>
        );
    }
}


const mapStateToProps = state => ({ regua:state.regua,usuario:state.usuario });

const mapDispatchToProps = dispatch =>  bindActionCreators(Actions, dispatch);


export default connect(mapStateToProps,mapDispatchToProps)(ConfigAccount)

const styles = StyleSheet.create({
    containerModal:{
        flex: 1,
        backgroundColor:'#eff0f1c9',
        maxHeight:Dimensions.get('window').height,
        height:'auto'
    },
    
});
  