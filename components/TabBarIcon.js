import React from 'react';
// import { Ionicons } from '@expo/vector-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';

import Colors from '../constants/Colors';

export default function TabBarIcon(props) {
  return (
    <Ionicons
      name={props.name}
      size={props.size == null ? 24 : props.size }
      style={{ marginBottom: -3 }}
      color={props.bottomBar == true ? (props.focused? Colors.tabIconSelected : Colors.tabIconDefault) : (props.color)}
    />
  );
}
