import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View,
  } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { AsyncStorage } from 'react-native';
import queryString from 'query-string';
import { showMessage, hideMessage } from "react-native-flash-message";



import FontAWIcon from '../components/FontAWIcon';
import ToggleSwitch from 'toggle-switch-react-native';
import CustomModal from './CustomModal';
import EditarNomeTomada from './EditarNomeTomada';


import * as Actions from '../redux/actions/ReduxActionController';

class Tomada extends Component {
    
    constructor(props) {
        super(props);
        this.modalTomada = React.createRef()
    }
    
    async _UpdateValues(){
        let valReturned = await this.putUpdateTomada(this.props.Tomada.key,+this.props.Tomada.status);
        // showMessage({
        //   message: "Sucesso",
        //   description: ""+valReturned.message,
        //   duration:2500,
        //   type: "default",
        //   backgroundColor: "#9FE3B4", // background color
        //   color: "#ffff", // text color
        // });
        if(valReturned.status == "ok"){
          this.props.UpdateStatusTomada(Object.assign({},this.props.regua),this.props.Tomada);     
        }else{
          showMessage({
            message: "Ocorreu um Erro",
            description: ""+valReturned.message,
            duration:2500,
            type: "default",
            backgroundColor: "#E77010", // background color
            color: "#ffff", // text color
          });
        }
       
    };

    async putUpdateTomada(idT,st) {
        try{
          let value = await fetch('https://www.brunos4ntos.com/arduino/recebePost.php', {
            method: 'POST',   
            body: queryString.stringify(
              {
                idTomada: idT,
                status: st,
              }
            ),
            headers:{
              'Content-Type': 'application/x-www-form-urlencoded'
            }
          })
          .then((response) => {
            return response.text();
          })
          .then((json) => {
            console.log(json);
            return json;
          })
          .catch((error) => {
            return {
              status:"error",
              message:"Erro ao Atualizar status da tomada durante a requisição"
            }
          });
          return JSON.parse(value)
        }catch(e){
          return {
            status:"error",
            message:"Não foi possivel enviar sua requisição"
          }
        }
        
    }

    render() {
        return (
          <View>
            <View style={{
                width:320,
                height:70,
                marginTop:20,
                borderColor: this.props.Tomada.status ? '#9FE3B4' : '#E77010',
                borderWidth:2,
                borderRadius:15,
                flexDirection: 'row',
                 justifyContent: 'space-between'
              }}>
                <TouchableWithoutFeedback onPress={()=>{
                  this.modalTomada.current.openModal({
                    title: 'Edite sua Tomada',
                    modalVisible: !this.modalTomada.current.state.modalVisible,
                    conteudo: EditarNomeTomada
                  });
                }}>
                  <View style={{flexDirection: 'row', justifyContent: 'space-between',width:230}} >
                    <View  style={{flexDirection: 'row', justifyContent: 'space-between',paddingTop:25,paddingLeft:10,paddingBottom:20,paddingRight:0}}>
                    <Text style={{fontSize:10,color: this.props.Tomada.status ? '#9FE3B4' : '#E77010',marginTop:-5}}>{this.props.Tomada.key}</Text>
                    <Text style={{ fontSize:14,fontWeight:'bold',color: this.props.Tomada.status ? '#9FE3B4' : '#E77010' }}>{this.props.Tomada.nome}</Text>
                    </View>
                    <View style={{paddingTop:0,paddingLeft:0,paddingBottom:0,paddingRight:0,flexDirection: 'column',alignItems:'center',alignSelf:'center',position:'absolute',margin:130}}>
                      <Text style={{ fontSize:14,fontWeight:'bold' }}>{this.props.Tomada.consumo} W</Text>
                      <Text style={{ fontSize:10 }}>Consumo</Text>
                    </View>
                  </View>
                </TouchableWithoutFeedback>
                <View style={{paddingTop:20,paddingLeft:0,paddingBottom:0,paddingRight:10}}>
                  <ToggleSwitch
                    isOn={this.props.Tomada.status}
                    onColor="#9FE3B4"
                    offColor="#C5C5C5"
                    disabled={false}
                    onToggle={async () => this._UpdateValues()}
                  />
                </View>
              
              </View>
              <CustomModal ref={this.modalTomada} props={{tomada:this.props.Tomada}}></CustomModal>
            </View>
        );
    }
}

const mapStateToProps = state => ({ regua:state.regua });

const mapDispatchToProps = dispatch =>  bindActionCreators(Actions, dispatch);

export default connect(mapStateToProps,mapDispatchToProps)(Tomada);

const styles = StyleSheet.create({
    headerContainer: {
      minHeight:160,
      padding:15,
      backgroundColor:'#9FE3B4'
    },
});
  