import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View,
  } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { AsyncStorage } from 'react-native';
import FontAWIcon from './FontAWIcon';
import queryString from 'query-string';
import ToggleSwitch from 'toggle-switch-react-native';
import { showMessage, hideMessage } from "react-native-flash-message";

import * as Actions from '../redux/actions/ReduxActionController';
import CustomModal from './CustomModal';
import EditarTimer from './EditarTimer';


class Timer extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            isOn: this.props.isOn === null ? false : this.props.Timer.status 
        };
        this.modalTimer = React.createRef()
    }
    
    async _UpdateValues(){
      console.log(this.props.Timer)
      let valReturned = await this.putUpdateTimer(this.props.Timer.idTimer,+(!this.props.Timer.status));
     
      if(valReturned.status == "ok"){
        this.props.UpdateStatusTimer(Object.assign({},this.props.regua),this.props.Timer);      
      }else{
        showMessage({
          message: "Ocorreu um Erro",
          description: ""+valReturned.message,
          duration:2500,
          type: "default",
          backgroundColor: "#E77010", // background color
          color: "#ffff", // text color
        });
      }
      this.setState({
          isOn:!this.props.Timer.status 
      });
    };

    async putUpdateTimer(idT,st) {
      try{
        let value = await fetch('https://www.brunos4ntos.com/arduino/timer/atualizaStatusTimer.php', {
          method: 'POST',   
          body: queryString.stringify(
            {
              idTimer: idT,
              status: st,
            }
          ),
          headers:{
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        })
        .then((response) => {
          return response.text();
        })
        .then((json) => {
          console.log(json);
          console.log(json);
          return json;
        })
        .catch((error) => {
          return {
            status:"error",
            message:"Erro ao Atualizar status da tomada durante a requisição"
          }
        });
        return JSON.parse(value)
      }catch(e){
        console.log(e);
        return {
          status:"error",
          message:"Não foi possivel enviar sua requisição"
        }
      }
      
  }

    render() {
        return (
          <View>
            <View style={{
                width:320,
                height:70,
                marginTop:20,
                borderColor: this.props.Timer.acao==true ? '#9FE3B4' : '#E77010',
                borderWidth:2,
                borderRadius:15,
                flexDirection: 'row',
                 justifyContent: 'space-between'
              }}>
                 <TouchableWithoutFeedback onPress={()=>{
                  this.modalTimer.current.openModal({
                    title: 'Edite sua Rotina',
                    modalVisible: !this.modalTimer.current.state.modalVisible,
                    conteudo: EditarTimer
                  });
                }}>
                  <View style={{flexDirection: 'row', justifyContent: 'space-between',width:230}}>
                    <View  style={{flexDirection: 'column', justifyContent: 'space-between',paddingTop:15,paddingLeft:30,paddingBottom:20,paddingRight:10}}>
                    <Text style={{ fontSize:10,fontWeight:'bold',color: this.props.Timer.acao==1 ? '#9FE3B4' : '#E77010' }}>{ this.props.Timer.acao==1?'Ligar':'Desligar'}</Text>
                    <Text style={{ fontSize:14,fontWeight:'bold',color: this.props.Timer.acao==1 ? '#9FE3B4' : '#E77010' }}>{ this.props.Timer.hora}</Text>
                    </View>
                    <View style={{paddingTop:0,paddingLeft:0,paddingBottom:0,paddingRight:0,flexDirection: 'column',alignItems:'center',alignSelf:'center',position:'absolute',margin:130}}>
                        <Text style={{ fontSize:10 }}>Tomadas</Text>
                        <Text style={{ fontSize:14,fontWeight:'bold' }}>{this.props.Timer.tomada1 != 0 && this.props.Timer.tomada2 != 0 ? '1 - 2' : (this.props.Timer.tomada1 != 0 ? '1' : '2')}</Text>      
                    </View>
                  </View>
                </TouchableWithoutFeedback>
                <View style={{paddingTop:20,paddingLeft:0,paddingBottom:0,paddingRight:10}}>
                  <ToggleSwitch
                    isOn={!!this.props.Timer.status}
                    onColor= {this.props.Timer.acao==true ?"#9FE3B4":'#E77010'}
                    offColor="#C5C5C5"
                    disabled={false}
                    onToggle={async () => this._UpdateValues()}
                  />
                </View>
                
              </View>
              <CustomModal ref={this.modalTimer} props={{Timer:this.props.Timer}}></CustomModal>
            </View>
        );
    }
}

const mapStateToProps = state => ({ regua:state.regua });

const mapDispatchToProps = dispatch =>  bindActionCreators(Actions, dispatch);

export default connect(mapStateToProps,mapDispatchToProps)(Timer);

const styles = StyleSheet.create({
    headerContainer: {
      minHeight:160,
      padding:15,
      backgroundColor:'#9FE3B4'
    },
});
  